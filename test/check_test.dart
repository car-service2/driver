import 'package:flutter_test/flutter_test.dart';

// Ваш простой калькулятор
class SimpleCalculator {
  int add(int a, int b) => a + b;
  int subtract(int a, int b) => a - b;
  int multiply(int a, int b) => a * b;
  double divide(int a, int b) => a / b;
}

void main() {
  test('Проверка операции сложения', () {
    // Создаем экземпляр калькулятора
    final calculator = SimpleCalculator();

    // Проверяем операцию сложения
    expect(calculator.add(5, 10), 15);
    expect(calculator.add(-5, 5), 0);
    expect(calculator.add(0, 0), 0);
  });

  test('Проверка операции вычитания', () {
    // Создаем экземпляр калькулятора
    final calculator = SimpleCalculator();

    // Проверяем операцию вычитания
    expect(calculator.subtract(10, 5), 5);
    expect(calculator.subtract(0, 5), -5);
    expect(calculator.subtract(-5, -5), 0);
  });

  test('Проверка операции умножения', () {
    // Создаем экземпляр калькулятора
    final calculator = SimpleCalculator();

    // Проверяем операцию умножения
    expect(calculator.multiply(2, 5), 10);
    expect(calculator.multiply(-2, 5), -10);
    expect(calculator.multiply(0, 5), 0);
  });

  test('Проверка операции деления', () {
    // Создаем экземпляр калькулятора
    final calculator = SimpleCalculator();

    // Проверяем операцию деления
    expect(calculator.divide(10, 2), 5);
    expect(calculator.divide(10, 3), closeTo(3.33, 0.01));
    expect(calculator.divide(0, 5), 0);
  });
}
