import Flutter
import CoreLocation

public class LocationStreamHandler: NSObject, FlutterStreamHandler {
    private let locationManager = CLLocationManager()
    private var eventSink: FlutterEventSink?

    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.distanceFilter = 50
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.allowsBackgroundLocationUpdates = true
    }

    public func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = events
        locationManager.startUpdatingLocation()
        return nil
    }

    public func onCancel(withArguments arguments: Any?) -> FlutterError? {
        locationManager.stopUpdatingLocation()
        eventSink = nil
        return nil
    }
}

extension LocationStreamHandler: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            eventSink?([
                "latitude": latitude,
                "longitude": longitude
            ])
        }
    }
}
