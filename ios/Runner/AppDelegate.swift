import UIKit
import Flutter
import flutter_background_service_ios // background service

@main
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    SwiftFlutterBackgroundServicePlugin.taskIdentifier = "dev.flutter.background.location"

      let flutterViewController: FlutterViewController = window?.rootViewController as! FlutterViewController

      // Создайте EventChannel и зарегистрируйте его.
      let locationChannel = FlutterEventChannel(
          name: "location_event_channel",
          binaryMessenger: flutterViewController.binaryMessenger
      )

      // Создайте экземпляр LocationStreamHandler без передачи аргумента.
      let locationStreamHandler = LocationStreamHandler()
      
      // Установите FlutterEventSink для LocationStreamHandler.
      locationChannel.setStreamHandler(locationStreamHandler)
      
      
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
