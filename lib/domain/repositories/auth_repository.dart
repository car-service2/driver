import 'package:driver/domain/entities/token.dart';

import '../entities/user.dart';

abstract class AuthRepository {
  Future<TokenEntity> logIn({required String login, required String password});

  Future<UserEntity> getUser();

  Future<void> logOut();
}
