import 'package:driver/domain/entities/statistic.dart';

abstract class StatisticsRepository {
  Future<StatisticEntity> daily();

  Future<StatisticEntity> custom({
    required String startDate,
    required String endDate,
  });
}
