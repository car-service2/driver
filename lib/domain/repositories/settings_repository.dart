import 'package:driver/domain/entities/address.dart';
import 'package:driver/domain/entities/user.dart';

abstract class SettingsRepository {
  Future<List<AddressEntity>> getAddresses();

  Future<bool> inLine();

  Future<UserEntity> updateStopAddress(int stopAddressId, DateTime dateTime);

  Future<void> setFCMToken(String token);

  Future<void> setLocation({required double longitude, required double latitude});
}
