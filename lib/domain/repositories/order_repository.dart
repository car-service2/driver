import 'package:driver/domain/entities/order.dart';

abstract class OrderRepository {
  Future<List<OrderEntity>> getOrders({
    required String status,
    required int page,
    required int perPage,
  });

  Future<void> accept({required int orderId});

  Future<void> decline({required int orderId});

  Future<void> endTrip({
    required int orderId,
    required double? orderValue,
    required double? orderCreditValue,
    required double? orderTolValue,
    String? description,
  });

  Future<void> start({required int orderId});

  Future<void> create(OrderEntity order);
}
