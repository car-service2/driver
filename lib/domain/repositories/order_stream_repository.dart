import 'package:driver/domain/entities/order.dart';

abstract class OrderStreamRepository {
  Stream<OrderEntity> getOrderStream();
}
