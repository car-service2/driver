import 'dart:async';

import 'package:driver/domain/repositories/order_stream_repository.dart';

import '../entities/order.dart';

class NewOrderUseCase {
  final OrderStreamRepository _orderStreamRepository;
  StreamSubscription<OrderEntity>? _orderSubscription;
  StreamController<OrderEntity>? _orderStreamController;

  NewOrderUseCase(this._orderStreamRepository);

  void startListeningForNewOrder({
    required Function(OrderEntity order) onNewOrderReceived,
    required Function(dynamic, StackTrace) onError,
  }) {
    stopListeningForNewOrder();
    _orderStreamController = StreamController<OrderEntity>.broadcast();
    _orderSubscription = _orderStreamRepository.getOrderStream().listen(
      (order) {
        _orderStreamController?.add(order);
        onNewOrderReceived(order);
      },
      onError: onError,
    );
  }

  void stopListeningForNewOrder() {
    _orderSubscription?.cancel();
    _orderSubscription = null;
    _orderStreamController?.close();
    _orderStreamController = null;
  }
}
