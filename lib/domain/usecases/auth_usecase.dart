import '../../data/datasources/interfaces/token_datasource.dart';
import '../entities/token.dart';
import '../entities/user.dart';
import '../repositories/auth_repository.dart';

class AuthUseCase {
  final AuthRepository _authRepository;
  final TokenDataSource _tokenStorage;

  AuthUseCase(this._authRepository, this._tokenStorage);

  Future<UserEntity> logIn({
    required String login,
    required String password,
  }) async {
    final TokenEntity token = await _authRepository.logIn(
      login: login,
      password: password,
    );

    bool isTokenSaved = await _tokenStorage.setToken(token.accessToken);
    if (isTokenSaved) {
      final UserEntity user = await _authRepository.getUser();
      return user;
    } else {
      throw Exception("Error saving token in token storage");
    }
  }

  Future<void> logOut() async {
    await _authRepository.logOut();
    final isTokenDeleted = await _tokenStorage.deleteToken();
    if (!isTokenDeleted) {
      throw Exception("Error saving token in token storage");
    }
  }

  Future<UserEntity> getUser() async {
    return await _authRepository.getUser();
  }
}
