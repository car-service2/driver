import 'package:driver/domain/entities/order.dart';
import 'package:driver/domain/repositories/order_repository.dart';

class OrderUseCase {
  final OrderRepository orderRepository;

  OrderUseCase(this.orderRepository);

  Future<List<OrderEntity>> getOrders({
    required String status,
    required int page,
    required int perPage,
  }) async {
    final orders = await orderRepository.getOrders(
      status: status,
      page: page,
      perPage: perPage,
    );

    return orders;
  }

  Future<void> endTrip({
    required int orderId,
    required double? orderValue,
    required double? orderCreditValue,
    required double? orderTolValue,
    String? description,
  }) async {
    return orderRepository.endTrip(
      orderId: orderId,
      orderValue: orderValue,
      description: description,
      orderCreditValue: orderCreditValue,
      orderTolValue: orderTolValue,
    );
  }

  Future<void> accept(int orderId) async {
    await orderRepository.accept(orderId: orderId);
  }

  Future<void> decline(int orderId) async {
    await orderRepository.decline(orderId: orderId);
  }

  Future<void> start(int orderId) async {
    await orderRepository.start(orderId: orderId);
  }

  Future<void> create(OrderEntity order) async {
    return await orderRepository.create(order);
  }
}
