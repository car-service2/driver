import 'package:driver/domain/entities/address.dart';
import 'package:driver/domain/repositories/settings_repository.dart';
import 'package:driver/firebase_messaging_service.dart';

import '../../data/datasources/local/fcm_token_storage.dart';
import '../entities/user.dart';

class SettingsUseCase {
  final SettingsRepository _settingsRepository;
  final FirebaseMessagingService _firebaseMessagingService;
  final FCMTokenStorage _fcmTokenStorage;

  SettingsUseCase(
    this._settingsRepository,
    this._firebaseMessagingService,
    this._fcmTokenStorage,
  );

  Future<bool> updateInline() async {
    return await _settingsRepository.inLine();
  }

  Future<List<AddressEntity>> getAddresses() async {
    return await _settingsRepository.getAddresses();
  }

  Future<UserEntity> updateStopAddress(int addressId, DateTime dateTime) async {
    return _settingsRepository.updateStopAddress(addressId, dateTime);
  }

  Future<void> setLocation({
    required double latitude,
    required double longitude,
  }) async {
    await _settingsRepository.setLocation(
      longitude: longitude,
      latitude: latitude,
    );
  }

  Future<bool> requestNotificationPermission() async {
    return await _firebaseMessagingService.requestPermission();
  }

  Future<void> setFSMToken() async {
    final fcmToken = await _firebaseMessagingService.getToken();
    if (fcmToken != null) {
      await _settingsRepository.setFCMToken(fcmToken);
      await _fcmTokenStorage.setToken(fcmToken);
    }
  }
}
