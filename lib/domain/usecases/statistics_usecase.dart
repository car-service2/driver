import 'package:driver/domain/repositories/statistics_repository.dart';

import '../entities/statistic.dart';

class StatisticsUseCase {
  final StatisticsRepository _statisticsRepository;

  StatisticsUseCase(this._statisticsRepository);

  Future<StatisticEntity> daily() async {
    return await _statisticsRepository.daily();
  }

  Future<StatisticEntity> custom({
    required String startDate,
    required String endDate,
  }) async {
    return _statisticsRepository.custom(
      startDate: startDate,
      endDate: endDate,
    );
  }
}
