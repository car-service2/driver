class UserEntity {
  final int id;
  final String name;
  final String login;
  final String? avatar;
  final bool inLine;
  final int? stopAddressId;

  UserEntity({
    required this.id,
    required this.name,
    required this.avatar,
    required this.inLine,
    required this.login,
    required this.stopAddressId,
  });
}
