import 'package:driver/data/models/waypoint.dart';
import 'package:driver/utils/date_time_extention.dart';
import 'package:flutter/material.dart';

class OrderEntity {
  final int id;
  final String customer;
  final String customerPhone;
  final String date;
  final OrderStatusEntity status;
  final num orderValue;
  final num orderCreditValue;
  final num orderTollValue;
  final List<Waypoint> waypoints;
  final String paymentType;
  final bool toll;
  final bool important;
  final String desciption;

  const OrderEntity({
    required this.id,
    required this.customer,
    required this.customerPhone,
    required this.date,
    required this.status,
    required this.orderValue,
    required this.orderCreditValue,
    required this.orderTollValue,
    required this.waypoints,
    required this.paymentType,
    required this.toll,
    required this.important,
    required this.desciption,
  });

  String get createdAt {
    return DateTime.fromMillisecondsSinceEpoch(int.parse(date) * 1000).format();
  }

  factory OrderEntity.fromDriver({
    required List<Waypoint> waypoints,
    required String date,
    required String phone,
    required double price,
    required String description,
  }) =>
      OrderEntity(
        id: 0,
        customer: '',
        customerPhone: phone,
        date: date,
        status: OrderStatusEntity.accept,
        waypoints: waypoints,
        paymentType: '',
        toll: false,
        important: false,
        desciption: description,
        orderValue: price,
        orderCreditValue: 0,
        //TODO
        orderTollValue: 0, //TODO
      );

  num get tripPrice =>
      paymentType.toLowerCase() == "cash" ? orderValue : orderCreditValue;
}

class OrderPoint {
  final String address;
  final int? rt;

  OrderPoint({
    required this.address,
    required this.rt,
  });
}

enum OrderStatusEntity {
  all,
  pending,
  accept,
  inProgress,
  success,
  canceled;

  @override
  String toString() {
    return switch (this) {
      OrderStatusEntity.all => "All",
      OrderStatusEntity.pending => "Pending",
      OrderStatusEntity.accept => "Accepted",
      OrderStatusEntity.inProgress => "In Progress",
      OrderStatusEntity.success => "Success",
      OrderStatusEntity.canceled => "Canceled",
    };
  }

  factory OrderStatusEntity.fromString(String status) {
    return switch (status) {
      "success" => OrderStatusEntity.success,
      "canceled" => OrderStatusEntity.canceled,
      "in_progress" => OrderStatusEntity.inProgress,
      "accept" => OrderStatusEntity.accept,
      "pending" => OrderStatusEntity.pending,
      _ => OrderStatusEntity.pending
    };
  }

  String toQueryParam() {
    return switch (this) {
      OrderStatusEntity.all => "",
      OrderStatusEntity.pending => "pending",
      OrderStatusEntity.accept => "accept",
      OrderStatusEntity.inProgress => "in_progress",
      OrderStatusEntity.success => "success",
      OrderStatusEntity.canceled => "canceled",
    };
  }

  Color get color {
    return switch (this) {
      OrderStatusEntity.all => Colors.white,
      OrderStatusEntity.pending => Colors.yellow.shade500,
      OrderStatusEntity.accept => Colors.amber,
      OrderStatusEntity.inProgress => Colors.greenAccent,
      OrderStatusEntity.success => Colors.green.shade500,
      OrderStatusEntity.canceled => Colors.red,
    };
  }

  OrderStatusEntity getNext() {
    int currentIndex = OrderStatusEntity.values.indexOf(this);
    int nextIndex = currentIndex == OrderStatusEntity.values.length - 1
        ? 0
        : currentIndex + 1;

    return OrderStatusEntity.values[nextIndex];
  }

  OrderStatusEntity getPrevious() {
    int currentIndex = OrderStatusEntity.values.indexOf(this);

    int nextIndex = currentIndex == 0
        ? OrderStatusEntity.values.length - 1
        : currentIndex - 1;

    return OrderStatusEntity.values[nextIndex];
  }
}
