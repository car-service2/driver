class TokenEntity {
  final String accessToken;
  final String tokenType;
  final int expiresIn;

  TokenEntity({
    required this.accessToken,
    required this.tokenType,
    required this.expiresIn,
  });

  factory TokenEntity.fromJson(Map<String, dynamic> json) {
    return TokenEntity(
      accessToken: json['access_token'] as String,
      tokenType: json['token_type'] as String,
      expiresIn: json['expires_in'] as int,
    );
  }
}
