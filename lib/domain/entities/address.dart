class AddressEntity {
  final int id;
  final String name;

  AddressEntity({
    required this.id,
    required this.name,
  });
}
