extension NumExtension on num {
  String formatWithTwoDecimals() {
    String formattedNumber = toStringAsFixed(2);

    if (formattedNumber.endsWith(".00")) {
      formattedNumber =
          formattedNumber.substring(0, formattedNumber.length - 3);
    }

    return formattedNumber;
  }
}
