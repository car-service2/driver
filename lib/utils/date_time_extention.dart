import 'package:intl/intl.dart';

extension Format on DateTime {
  String format() {
    final DateFormat formatter = DateFormat("MMM d, y | ")..add_jm();
    return formatter.format(this);
  }

  String toYMD() {
    return DateFormat('yyyy-MM-dd').format(this);
  }
}
