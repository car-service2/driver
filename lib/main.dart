import 'package:driver/service_locator.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'application.dart';
import 'background_service.dart';
import 'firebase_messaging_service.dart';
import 'firebase_options.dart';
import 'presentation/blocs/addresses/addresses_cubit.dart';
import 'presentation/blocs/auth/auth_bloc.dart';
import 'presentation/blocs/inline/inline_cubit.dart';
import 'presentation/blocs/login/login_cubit.dart';
import 'presentation/blocs/logout/logout_cubit.dart';
import 'presentation/blocs/menu/menu_cubit.dart';
import 'presentation/blocs/new_order/new_order_bloc.dart';
import 'presentation/blocs/order_status/order_status_cubit.dart';
import 'presentation/blocs/orders/orders_cubit.dart';
import 'presentation/blocs/permissions/permissions_cubit.dart';
import 'presentation/blocs/statistics/statistics_cubit.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  await initializeService();
  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          create: (_) => locator<AuthBloc>()..add(AuthCheckAuthenticationEvent()),
        ),
        BlocProvider<LoginCubit>(
          create: (_) => locator<LoginCubit>(),
        ),
        BlocProvider<LogoutCubit>(
          create: (_) => locator<LogoutCubit>(),
        ),
        BlocProvider<MenuCubit>(
          create: (_) => locator<MenuCubit>(),
        ),
        BlocProvider<AddressesCubit>(
          create: (_) => locator<AddressesCubit>(),
        ),
        BlocProvider<InlineCubit>(
          create: (_) => locator<InlineCubit>(),
        ),
        BlocProvider<OrdersCubit>(
          create: (_) => locator<OrdersCubit>(),
        ),
        BlocProvider<OrderStatusCubit>(
          create: (_) => locator<OrderStatusCubit>(),
        ),
        BlocProvider<StatisticsDailyCubit>(
          create: (_) => locator<StatisticsDailyCubit>(),
        ),
        BlocProvider<StatisticsCustomCubit>(
          create: (_) => locator<StatisticsCustomCubit>(),
        ),
        BlocProvider<NewOrderBloc>(
          create: (_) => locator<NewOrderBloc>(),
        ),
        BlocProvider<PermissionsCubit>(
          create: (_) => locator<PermissionsCubit>(),
        ),
      ],
      child: const Application(),
    ),
  );
}
