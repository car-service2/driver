import 'package:flutter/material.dart';

class ConfirmDialog {
  static Future<bool?> show({
    required context,
    required String title,
    required String content,
  }) async {
    return await showAdaptiveDialog<bool>(
      context: context,
      builder: (_) {
        return AlertDialog.adaptive(
          title: Text(title),
          content: Text(content),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text("Yes"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text("No"),
            )
          ],
        );
      },
    );
  }
}
