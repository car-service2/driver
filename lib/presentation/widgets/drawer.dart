import 'package:driver/presentation/blocs/auth/auth_bloc.dart';
import 'package:driver/presentation/blocs/inline/inline_cubit.dart';
import 'package:driver/presentation/blocs/logout/logout_cubit.dart';
import 'package:driver/presentation/blocs/menu/menu_cubit.dart';
import 'package:driver/presentation/theme/app_theme.dart';
import 'package:driver/presentation/widgets/confirm_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
        bottom: false,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BlocBuilder<AuthBloc, AuthState>(
                builder: (context, state) {
                  if (state is AuthAuthenticated) {
                    return Row(
                      children: [
                        SizedBox.square(
                          dimension: 50,
                          child: state.user.avatar == null
                              ? const Icon(
                                  Icons.account_circle,
                                  size: 50,
                                  color: Colors.white,
                                )
                              : ClipRRect(
                                  borderRadius: BorderRadius.circular(50),
                                  child: Image.network(
                                    state.user.avatar!,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            state.user.name,
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                            ),
                            maxLines: 2,
                          ),
                        ),
                      ],
                    );
                  }
                  return const SizedBox(height: 50);
                },
              ),
              const SizedBox(height: 40),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  BlocProvider.of<MenuCubit>(context).set(Menu.settings);
                },
                contentPadding: EdgeInsets.zero,
                leading: SvgPicture.asset("assets/images/online.svg"),
                title: Text(
                  "Online",
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                  ),
                ),
                trailing: BlocBuilder<InlineCubit, InlineState>(
                  buildWhen: (prev, next) => next is InlineChanged,
                  builder: (context, state) {
                    if (state is InlineChanged) {
                      return SizedBox(
                        height: 32,
                        width: 60,
                        child: CupertinoSwitch(
                          value: state.inline,
                          trackColor: Colors.white,
                          activeColor: Colors.white,
                          thumbColor:
                              state.inline ? AppColors.background : Colors.grey,
                          onChanged: (bool value) {
                            BlocProvider.of<InlineCubit>(context).change();
                          },
                        ),
                      );
                    }
                    return const SizedBox();
                  },
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.zero,
                onTap: () {
                  Navigator.pop(context);
                  BlocProvider.of<MenuCubit>(context).set(Menu.orders);
                },
                leading: SvgPicture.asset("assets/images/orders.svg"),
                title: Text(
                  "Orders",
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                  ),
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.zero,
                onTap: () {
                  Navigator.pop(context);
                  BlocProvider.of<MenuCubit>(context).set(Menu.statistics);
                },
                leading: SvgPicture.asset("assets/images/statistics.svg"),
                title: Text(
                  "Statistic",
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                  ),
                ),
              ),
              const Spacer(),
              BlocConsumer<LogoutCubit, LogoutState>(
                listener: (context, state) {
                  if (state is LogoutFailure) {
                    Navigator.pop(context);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(state.message),
                      ),
                    );
                  }
                },
                builder: (context, state) {
                  return ListTile(
                    contentPadding: EdgeInsets.zero,
                    onTap: () {
                      ConfirmDialog.show(
                        context: context,
                        title: "Log out",
                        content: "Are you sure?",
                      ).then((logout) {
                        if (logout == true) {
                          BlocProvider.of<LogoutCubit>(context).logout();
                        }
                      });
                    },
                    leading: state is LogoutLoading
                        ? const SizedBox.square(
                            dimension: 20,
                            child: CircularProgressIndicator(
                              strokeWidth: 1.5,
                              color: Colors.white,
                            ),
                          )
                        : SvgPicture.asset("assets/images/logout.svg"),
                    title: Text(
                      "Log Out",
                      style: GoogleFonts.montserrat(
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
