import 'package:driver/presentation/widgets/choose_address_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../domain/entities/order.dart';
import '../blocs/order_status/order_status_cubit.dart';
import '../blocs/orders/orders_cubit.dart';
import '../theme/app_theme.dart';
import 'button_widget.dart';
import 'confirm_dialog.dart';
import 'finish_order_dialog.dart';
import 'new_order_dialog.dart';
import 'outlined_button_widget.dart';

class OrderWidget extends StatefulWidget {
  final OrderEntity order;

  const OrderWidget({
    super.key,
    required this.order,
  });

  @override
  State<OrderWidget> createState() => _OrderWidgetState();
}

class _OrderWidgetState extends State<OrderWidget> {
  OrderEntity get order => widget.order;

  bool opened = false;

  late OrdersCubit ordersCubit;
  late OrderStatusCubit orderStatusCubit;

  @override
  void initState() {
    ordersCubit = BlocProvider.of<OrdersCubit>(context);
    orderStatusCubit = BlocProvider.of<OrderStatusCubit>(context);
    super.initState();
  }

  Widget _buildCustomerInfo() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(22),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  order.waypoints.firstOrNull?.address ?? "-",
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                  maxLines: 3,
                ),
              ),
              if (order.status == OrderStatusEntity.accept)
                GestureDetector(
                  onTap: _onTel,
                  child: Container(
                    height: 52,
                    width: 52,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: Center(
                      child: SvgPicture.asset(
                        "assets/images/phone.svg",
                      ),
                    ),
                  ),
                )
            ],
          ),
          const SizedBox(height: 8),
          Text(
            "Date & time",
            style: GoogleFonts.montserrat(
              fontWeight: FontWeight.w500,
              fontSize: 12,
              color: const Color(0xFF0D0D0D).withOpacity(0.4),
            ),
          ),
          const SizedBox(height: 4),
          Row(
            children: [
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 4,
                  horizontal: 8,
                ),
                decoration: BoxDecoration(
                  color: const Color(0xFFF6F6F6),
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Text(
                  order.createdAt,
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                    color: const Color(0xFF0D0D0D),
                  ),
                ),
              ),
              const Spacer(),
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 4,
                  horizontal: 8,
                ),
                decoration: BoxDecoration(
                  color: order.status.color,
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Text(
                  order.status.toString(),
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
          if (order.desciption.isNotEmpty) ...[
            const SizedBox(height: 5),
            Text(
              "Description",
              style: GoogleFonts.montserrat(
                fontWeight: FontWeight.w500,
                fontSize: 12,
                color: const Color(0xFF0D0D0D).withOpacity(0.4),
              ),
            ),
            const SizedBox(height: 4),
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 4,
                horizontal: 8,
              ),
              decoration: BoxDecoration(
                color: const Color(0xFFF6F6F6),
                borderRadius: BorderRadius.circular(6),
              ),
              child: Text(
                order.desciption,
                style: GoogleFonts.montserrat(
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                  color: const Color(0xFF0D0D0D),
                ),
              ),
            ),
          ]
        ],
      ),
    );
  }

  List<Widget> _buildShowMoreInfo() {
    return [
      const SizedBox(height: 14),
      Container(
        padding: const EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(22),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "ID:",
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                    color: const Color(0xFF0D0D0D).withOpacity(0.4),
                  ),
                ),
                const SizedBox(width: 8),
                Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 4,
                    horizontal: 8,
                  ),
                  decoration: BoxDecoration(
                    color: const Color(0xFFF6F6F6),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Text(
                    order.id.toString(),
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: const Color(0xFF0D0D0D),
                    ),
                  ),
                ),
                const Spacer(),
                SvgPicture.asset("assets/images/cash.svg"),
                const SizedBox(width: 8),
                Text(
                  "${order.paymentType} | ${order.tripPrice} USD",
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                    color: const Color(0xFF0D0D0D),
                  ),
                )
              ],
            ),
            if (order.toll)
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  const Icon(
                    Icons.toll,
                    color: Colors.amber,
                  ),
                  const SizedBox(width: 10),
                  Text(
                    "Toll",
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: const Color(0xFF0D0D0D),
                    ),
                  ),
                ],
              ),
            if (order.important)
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  const Icon(
                    Icons.priority_high,
                    color: Colors.red,
                  ),
                  Text(
                    "Priority High",
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: const Color(0xFF0D0D0D),
                    ),
                  ),
                ],
              ),
            const SizedBox(height: 10),
            Divider(
              color: const Color(0xFF0D0D0D).withOpacity(0.1),
              height: 0,
              thickness: 1,
            ),
            const SizedBox(height: 20),
            if (order.waypoints.isNotEmpty)
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 4,
                  horizontal: 8,
                ),
                margin: const EdgeInsets.only(bottom: 5),
                decoration: BoxDecoration(
                  color: const Color(0xFFF6F6F6),
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Text(
                  "Passengers: ${order.waypoints.first.passengers}",
                  style: GoogleFonts.montserrat(
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                    color: const Color(0xFF0D0D0D),
                  ),
                ),
              ),
            ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (_, index) {
                var point = order.waypoints.elementAt(index);
                var pointTitle = '';
                if (point == order.waypoints.first) {
                  pointTitle = 'start';
                } else if (point == order.waypoints.last) {
                  pointTitle = 'finish';
                } else {
                  pointTitle = 'intermediate';
                }
                return Row(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: const Color(0xFF57BF9C).withOpacity(0.3),
                      ),
                      child: Center(
                        child: Container(
                          height: 38,
                          width: 38,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: const Color(0xFF57BF9C),
                          ),
                          child: Icon(
                            point == order.waypoints.firstOrNull
                                ? Icons.location_searching
                                : Icons.location_on,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 12),
                    Flexible(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  pointTitle,
                                  style: GoogleFonts.montserrat(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: const Color(0xFF0D0D0D)
                                        .withOpacity(0.4),
                                  ),
                                ),
                                Text(
                                  point.address,
                                  style: GoogleFonts.montserrat(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Waiting:",
                                      style: GoogleFonts.montserrat(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 14,
                                        color: const Color(0xFF0D0D0D)
                                            .withOpacity(0.4),
                                      ),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 4,
                                        horizontal: 8,
                                      ),
                                      decoration: BoxDecoration(
                                        color: const Color(0xFFF6F6F6),
                                        borderRadius: BorderRadius.circular(6),
                                      ),
                                      child: Text(
                                        "${point.rt} min.",
                                        style: GoogleFonts.montserrat(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 14,
                                          color: const Color(0xFF0D0D0D),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              Clipboard.setData(
                                ClipboardData(text: point.address),
                              ).then((value) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text("Address copied")),
                                );
                              });
                            },
                            child: const Icon(Icons.copy),
                          ),
                        ],
                      ),
                    )
                  ],
                );
              },
              separatorBuilder: (_, index) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 10, top: 10),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: SizedBox(
                      width: 50,
                      child: SvgPicture.asset(
                        "assets/images/vertical-devider.svg",
                      ),
                    ),
                  ),
                );
              },
              itemCount: order.waypoints.length,
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: TextButton.icon(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: AppColors.background.withOpacity(.2),
                      foregroundColor: AppColors.background,
                      maximumSize: const Size.fromHeight(40),
                      minimumSize: const Size.fromHeight(40),
                    ),
                    onPressed: () async {
                      if (order.waypoints.isEmpty) return;

                      var location = order.waypoints.first;

                      launchWaze(location.latitude, location.longitude);
                    },
                    icon: const Icon(Icons.directions),
                    label: const Text("Waze"),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextButton.icon(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: AppColors.background.withOpacity(.2),
                      foregroundColor: AppColors.background,
                      maximumSize: const Size.fromHeight(40),
                      minimumSize: const Size.fromHeight(40),
                    ),
                    onPressed: () async {
                      if (order.waypoints.isEmpty) return;

                      var location = order.waypoints.first;
                      launchGoogleMaps(location.latitude, location.longitude);
                    },
                    icon: const Icon(Icons.directions),
                    label: const Text("Google Maps"),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ];
  }

  Widget _buildAcceptAndCancelButtons() {
    return Row(
      children: [
        Expanded(
          child: SizedBox(
            height: 50,
            child: ButtonWidget(
              title: "Accept",
              onPressed: onAccept,
              loading: false,
              background: const Color(0xFF00AE74),
            ),
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: SizedBox(
            height: 50,
            child: OutlinedButtonWidget(
              title: "Decline",
              onPressed: onDecline,
              loading: false,
              background: Colors.black,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildStartAndCancelButtons() {
    return Row(
      children: [
        Expanded(
          child: SizedBox(
            height: 50,
            child: ButtonWidget(
              title: "Start trip",
              onPressed: onStart,
              loading: false,
            ),
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: SizedBox(
            height: 50,
            child: BlocBuilder<OrderStatusCubit, OrderStatusState>(
              builder: (context, state) {
                return OutlinedButtonWidget(
                  title: "Cancel",
                  onPressed: onDecline,
                  loading: state is OrderStatusChanging,
                  background: const Color(0xFFEE4646),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      padding: const EdgeInsets.all(14),
      decoration: BoxDecoration(
        color: const Color(0xFFF6F6F6),
        borderRadius: BorderRadius.circular(34),
      ),
      child: Column(
        children: [
          GestureDetector(
            onTap: _onOpenClose,
            child: _buildCustomerInfo(),
          ),
          if (opened) ..._buildShowMoreInfo(),
          const SizedBox(height: 14),
          if (order.status == OrderStatusEntity.pending)
            _buildAcceptAndCancelButtons(),
          if (order.status == OrderStatusEntity.accept)
            _buildStartAndCancelButtons(),
          if (order.status == OrderStatusEntity.inProgress)
            SizedBox(
              height: 50,
              child: ButtonWidget(
                title: "End trip",
                onPressed: onFinish,
                loading: false,
                background: const Color(0xFF00AE74),
              ),
            ),
          GestureDetector(
            onTap: _onOpenClose,
            child: SvgPicture.asset(
              opened
                  ? "assets/images/arrow_up.svg"
                  : "assets/images/arrow_down.svg",
            ),
          ),
        ],
      ),
    );
  }

  void _onOpenClose() => setState(() => opened = !opened);

  void _onTel() async {
    await launchUrl(Uri.parse("tel:${order.customerPhone}"));
  }

  Future<void> onAccept() async {
    bool? isAccepted = await NewOrderDialog.show<bool>(
      context: context,
      order: order,
      onAccept: () async {
        Navigator.pop(context, true);
      },
      onDecline: () async {
        Navigator.pop(context, false);
      },
    );

    if (isAccepted != null) {
      if (isAccepted) {
        await orderStatusCubit.accept(orderId: order.id);
        ordersCubit.applyFilter(OrderStatusEntity.accept);
      } else {
        await orderStatusCubit.decline(orderId: order.id);
        ordersCubit.applyFilter(OrderStatusEntity.canceled);
      }
    }
  }

  void onDecline() {
    ConfirmDialog.show(
      context: context,
      title: "Decline",
      content: "Are you sure?",
    ).then((decline) {
      if (decline == true) {
        orderStatusCubit.decline(orderId: order.id).then((value) {
          ordersCubit.applyFilter(OrderStatusEntity.canceled);
          ChooseAddressBottomSheet.show(
            context: context,
            showCurrentLocation: false,
          );
        });
      }
    });
  }

  void onStart() async {
    bool? startTrip = await ConfirmDialog.show(
      context: context,
      title: "Start trip",
      content: "Are you sure?",
    );
    if (startTrip == true) {
      await orderStatusCubit.start(orderId: order.id);
      ordersCubit.applyFilter(OrderStatusEntity.inProgress);
    }
  }

  Future<void> onFinish() async {
    final bool? finished = await FinishOrderDialog.show<bool>(
      context: context,
      order: order,
    );

    if (finished == true) {
      ordersCubit.applyFilter(OrderStatusEntity.all);
    }
  }

  // open map & navigator
  void launchWaze(double lat, double lng) async {
    var url = 'waze://?ll=${lat.toString()},${lng.toString()}';
    var fallbackUrl =
        'https://waze.com/ul?ll=${lat.toString()},${lng.toString()}&navigate=yes';
    try {
      bool launched =
          await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
      if (!launched) {
        await launchUrl(Uri.parse(fallbackUrl));
      }
    } catch (e) {
      await launchUrl(Uri.parse(fallbackUrl));
    }
  }

  void launchGoogleMaps(double lat, double lng) async {
    var url = 'google.navigation:q=${lat.toString()},${lng.toString()}';
    var fallbackUrl =
        'https://www.google.com/maps/search/?api=1&query=${lat.toString()},${lng.toString()}';
    try {
      bool launched =
          await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
      if (!launched) {
        await launchUrl(Uri.parse(fallbackUrl));
      }
    } catch (e) {
      await launchUrl(Uri.parse(fallbackUrl));
    }
  }
}
