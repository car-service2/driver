import 'package:driver/presentation/widgets/choose_address_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../domain/entities/order.dart';
import '../blocs/order_status/order_status_cubit.dart';
import 'button_widget.dart';

class FinishOrderDialog extends StatefulWidget {
  final OrderEntity order;

  const FinishOrderDialog._({
    required this.order,
  });

  @override
  State<FinishOrderDialog> createState() => _FinishOrderDialogState();

  static Future<T?> show<T>({
    required BuildContext context,
    required OrderEntity order,
  }) async {
    return showDialog<T>(
      context: context,
      builder: (_) {
        return FinishOrderDialog._(order: order);
      },
    );
  }
}

class _FinishOrderDialogState extends State<FinishOrderDialog> {
  TextEditingController priceController = TextEditingController();
  TextEditingController creditController = TextEditingController();
  TextEditingController tollController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  bool get validated {
    return priceController.text.isNotEmpty || creditController.text.isNotEmpty;
  }

  OutlineInputBorder get border => OutlineInputBorder(
        borderRadius: BorderRadius.circular(12.0),
        borderSide: const BorderSide(color: Colors.white, width: 0),
      );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    priceController.dispose();
    creditController.dispose();
    tollController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Dialog(
        insetPadding: const EdgeInsets.all(20),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(34),
        ),
        child: FittedBox(
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(22),
            decoration: BoxDecoration(
              color: const Color(0xFFF6F6F6),
              borderRadius: BorderRadius.circular(34),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(22),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.order.waypoints.firstOrNull?.address ?? "-",
                        style: GoogleFonts.montserrat(
                          fontWeight: FontWeight.w700,
                          fontSize: 14,
                        ),
                        maxLines: 3,
                      ),
                      const SizedBox(height: 5),
                      if (widget.order.customer.isNotEmpty) ...[
                        Text(
                          "Customer",
                          style: GoogleFonts.montserrat(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                            color: const Color(0xFF0D0D0D).withOpacity(0.4),
                          ),
                        ),
                        Text(
                          widget.order.customer,
                          style: GoogleFonts.montserrat(
                            fontWeight: FontWeight.w700,
                            fontSize: 18,
                          ),
                          maxLines: 1,
                        ),
                        const SizedBox(height: 20),
                      ],
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Date & time",
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              color: const Color(0xFF0D0D0D).withOpacity(0.4),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(
                              vertical: 4,
                              horizontal: 8,
                            ),
                            decoration: BoxDecoration(
                              color: const Color(0xFFF6F6F6),
                              borderRadius: BorderRadius.circular(6),
                            ),
                            child: Text(
                              widget.order.createdAt,
                              style: GoogleFonts.montserrat(
                                fontWeight: FontWeight.w600,
                                fontSize: 14,
                                color: const Color(0xFF0D0D0D),
                              ),
                            ),
                          ),
                        ],
                      ),
                      if (widget.order.desciption.isNotEmpty) ...[
                        const SizedBox(height: 5),
                        Text(
                          "Description",
                          style: GoogleFonts.montserrat(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                            color: const Color(0xFF0D0D0D).withOpacity(0.4),
                          ),
                        ),
                        const SizedBox(height: 4),
                        Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 4,
                            horizontal: 8,
                          ),
                          decoration: BoxDecoration(
                            color: const Color(0xFFF6F6F6),
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: Text(
                            widget.order.desciption,
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: const Color(0xFF0D0D0D),
                            ),
                          ),
                        ),
                      ],
                    ],
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  "Set the trip price",
                  style: GoogleFonts.montserrat(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 8),
                if (widget.order.paymentType == 'cash') ...[
                  TextField(
                    controller: priceController,
                    keyboardType: TextInputType.number,
                    onChanged: (value) => setState(() {}),
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                      hintText: widget.order.orderValue.toString(),
                      filled: true,
                      fillColor: Colors.white,
                      hintStyle: GoogleFonts.montserrat(
                        fontWeight: FontWeight.w600,
                        color: const Color(0xFF0D0D0D).withOpacity(0.1),
                      ),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "USD",
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      prefixIcon: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset("assets/images/wallet.svg"),
                        ],
                      ),
                      border: border,
                      errorBorder: border,
                      focusedBorder: border,
                      focusedErrorBorder: border,
                      enabledBorder: border,
                      disabledBorder: border,
                    ),
                  ),
                  const SizedBox(height: 8),
                ],
                if (widget.order.paymentType == 'credit') ...[
                  Text(
                    "If the entered price does not match the predetermined price, indicate the reason",
                    style: GoogleFonts.montserrat(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: const Color(0xFF0D0D0D).withOpacity(0.5),
                      height: 17 / 14,
                    ),
                  ),
                  const SizedBox(height: 8),
                  TextField(
                    controller: creditController,
                    keyboardType: TextInputType.number,
                    onChanged: (value) => setState(() {}),
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                      hintText: widget.order.orderCreditValue.toString(),
                      filled: true,
                      fillColor: Colors.white,
                      hintStyle: GoogleFonts.montserrat(
                        fontWeight: FontWeight.w600,
                        color: const Color(0xFF0D0D0D).withOpacity(0.1),
                      ),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "USD",
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      prefixIcon: const Icon(
                        Icons.credit_card,
                        color: Colors.black,
                      ),
                      border: border,
                      errorBorder: border,
                      focusedBorder: border,
                      focusedErrorBorder: border,
                      enabledBorder: border,
                      disabledBorder: border,
                    ),
                  ),
                  const SizedBox(height: 8),
                  TextField(
                    controller: priceController,
                    keyboardType: TextInputType.number,
                    onChanged: (value) => setState(() {}),
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                      hintText: widget.order.orderValue.toString(),
                      filled: true,
                      fillColor: Colors.white,
                      hintStyle: GoogleFonts.montserrat(
                        fontWeight: FontWeight.w600,
                        color: const Color(0xFF0D0D0D).withOpacity(0.1),
                      ),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "USD",
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      prefixIcon: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset("assets/images/wallet.svg"),
                        ],
                      ),
                      border: border,
                      errorBorder: border,
                      focusedBorder: border,
                      focusedErrorBorder: border,
                      enabledBorder: border,
                      disabledBorder: border,
                    ),
                  ),
                  const SizedBox(height: 8),
                  TextField(
                    controller: tollController,
                    keyboardType: TextInputType.number,
                    onChanged: (value) => setState(() {}),
                    style: GoogleFonts.montserrat(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                      hintText: widget.order.orderTollValue.toString(),
                      filled: true,
                      fillColor: Colors.white,
                      hintStyle: GoogleFonts.montserrat(
                        fontWeight: FontWeight.w600,
                        color: const Color(0xFF0D0D0D).withOpacity(0.1),
                      ),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "USD",
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      prefixIcon: const Icon(Icons.toll, color: Colors.black),
                      border: border,
                      errorBorder: border,
                      focusedBorder: border,
                      focusedErrorBorder: border,
                      enabledBorder: border,
                      disabledBorder: border,
                    ),
                  ),
                  const SizedBox(height: 8),
                ],
                const SizedBox(height: 14),
                BlocConsumer<OrderStatusCubit, OrderStatusState>(
                  listener: (context, state) {
                    if (state is OrderStatusFailure) {
                      SnackBar snackBar = SnackBar(
                        content: Text(state.message),
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  },
                  builder: (context, state) {
                    return ButtonWidget(
                      title: "End trip",
                      background: Colors.black,
                      onPressed: validated
                          ? () {
                              double? price =
                                  double.tryParse(priceController.text);
                              double? orderCredit =
                                  double.tryParse(creditController.text);
                              double? orderToll =
                                  double.tryParse(tollController.text);
                              BlocProvider.of<OrderStatusCubit>(context)
                                  .endTrip(
                                orderId: widget.order.id,
                                description: descriptionController.text,
                                orderValue: price,
                                orderCreditValue: orderCredit,
                                orderTolValue: orderToll,
                              )
                                  .then(
                                (value) {
                                  Navigator.pop(context, true);
                                  ChooseAddressBottomSheet.show(
                                    context: context,
                                    showCurrentLocation: false,
                                  );
                                },
                              );
                            }
                          : null,
                      loading: state is OrderStatusChanging,
                    );
                  },
                ),
                const SizedBox(height: 14),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
