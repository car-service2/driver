import 'package:driver/domain/entities/address.dart';
import 'package:driver/presentation/pages/orders_page/set_date_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class SetArriveTimeDialog {
  static Future<T?> show<T>(
    BuildContext context,
    AddressEntity addressEntity,
  ) async {
    return await showDialog(
      context: context,
      builder: (_) {
        return SetDateTimeDialog(location: addressEntity.name);
      },
    );
  }
}

class SetDateTimeDialog extends StatefulWidget {
  final String location;

  const SetDateTimeDialog({super.key, required this.location});

  @override
  State<SetDateTimeDialog> createState() => _SetDateTimeDialogState();
}

class _SetDateTimeDialogState extends State<SetDateTimeDialog> {
  DateTime dateTime = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(20),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                SvgPicture.asset("assets/images/location.svg"),
                const SizedBox(width: 10),
                Text(
                  widget.location,
                  style: GoogleFonts.montserrat(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                )
              ],
            ),
            const Divider(),
            SetDateWidget(
              title: "Set arrive time",
              dateTime: dateTime,
              onChanged: (value) {
                dateTime = value;
                setState(() {});
              },
            ),
            const Divider(),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: const Size.fromHeight(40),
              ),
              onPressed: dateTime.isAfter(
                DateTime.now().subtract(const Duration(minutes: 1)),
              )
                  ? () => Navigator.pop(context, dateTime)
                  : null,
              child: const Text("Set"),
            )
          ],
        ),
      ),
    );
  }
}
