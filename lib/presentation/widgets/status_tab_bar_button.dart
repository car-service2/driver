import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StatusTabButton extends StatelessWidget {
  final bool selected;
  final VoidCallback onPressed;
  final String title;

  const StatusTabButton({
    super.key,
    required this.selected,
    required this.onPressed,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
        backgroundColor: selected ? Colors.white : null,
        foregroundColor: selected ? Colors.black : Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        side: BorderSide(
          color: selected ? Colors.white : Colors.white.withOpacity(0.4),
        ),
        padding: const EdgeInsets.symmetric(
          vertical: 14,
          horizontal: 18,
        ),
        textStyle: GoogleFonts.montserrat(
          fontSize: 16,
          fontWeight: selected ? FontWeight.w600 : FontWeight.w400,
        ),
      ),
      onPressed: onPressed,
      child: Text(title),
    );
  }
}