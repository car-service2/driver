import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../blocs/menu/menu_cubit.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(30.0),
        topRight: Radius.circular(30.0),
      ),
      child: BlocBuilder<MenuCubit, Menu>(
        builder: (context, menu) {
          return BottomNavigationBar(
            currentIndex: Menu.values.indexOf(menu),
            onTap: (index) {
              BlocProvider.of<MenuCubit>(context)
                  .set(Menu.values.elementAt(index));
            },
            items: [
              BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/images/my-orders.svg",
                ),
                label: "My orders",
                activeIcon: SvgPicture.asset(
                  "assets/images/my-orders-active.svg",
                ),
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset("assets/images/statistic.svg"),
                activeIcon:
                    SvgPicture.asset("assets/images/statistic-active.svg"),
                label: "Statistics",
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset("assets/images/settings.svg"),
                activeIcon:
                    SvgPicture.asset("assets/images/settings-active.svg"),
                label: "Driver",
              ),
            ],
          );
        },
      ),
    );
  }
}
