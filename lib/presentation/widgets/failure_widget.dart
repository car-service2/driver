import 'package:flutter/material.dart';

class FailureWidget extends StatelessWidget {
  final String message;
  final VoidCallback onPressed;

  const FailureWidget({
    super.key,
    required this.message,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(20),
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: const Color(0xFFF6F6F6),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: [
          Text(message),
          ElevatedButton(
            onPressed: onPressed,
            child: const Text("Retry"),
          )
        ],
      ),
    );
  }
}
