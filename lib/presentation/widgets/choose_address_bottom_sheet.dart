import 'package:driver/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:driver/presentation/theme/app_theme.dart';
import 'package:driver/presentation/widgets/set_arrive_time_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../pages/settings_page.dart';

class ChooseAddressBottomSheet extends StatelessWidget {
  final bool showCurrentLocation;

  static Future<T?> show<T>({
    required BuildContext context,
    required bool showCurrentLocation,
  }) async {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      useSafeArea: true,
      enableDrag: false,
      constraints: BoxConstraints(
        maxHeight: MediaQuery.of(context).size.height * .8,
        minHeight: MediaQuery.of(context).size.height / 2,
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      builder: (_) {
        return ChooseAddressBottomSheet._(showCurrentLocation);
      },
    );
  }

  const ChooseAddressBottomSheet._(this.showCurrentLocation);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: [
          Row(
            children: [
              SvgPicture.asset("assets/images/location.svg"),
              const SizedBox(width: 10),
              Text(
                "Choose were you are",
                style: GoogleFonts.montserrat(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          const Divider(thickness: 1),
          const SizedBox(height: 10),
          BlocConsumer<AddressesCubit, AddressesState>(
            listener: (context, state) {
              if (state is AddressesFailure) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text(state.message)),
                );
              }
            },
            builder: (context, state) {
              if (state is AddressesLoaded) {
                return Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: state.addresses
                          .map(
                            (address) => CheckBoxWidget(
                              title: address.name,
                              value: showCurrentLocation &&
                                  state.selectedAddress?.name == address.name,
                              onChanged: (value) async {
                                SetArriveTimeDialog.show(context, address)
                                    .then((datetime) {
                                  BlocProvider.of<AddressesCubit>(context)
                                      .select(address, datetime);
                                  if (datetime != null) {
                                    Navigator.pop(context);
                                  }
                                });
                              },
                              active: true,
                            ),
                          )
                          .toList(),
                    ),
                  ),
                );
              }

              return Expanded(
                child: Center(
                  child: SizedBox.square(
                    dimension: 20,
                    child: CircularProgressIndicator(
                      strokeWidth: 1.5,
                      color: AppColors.background,
                    ),
                  ),
                ),
              );
            },
          ),
          const SizedBox(height: 10),
          SizedBox(
            height: 50,
            width: MediaQuery.of(context).size.width,
            child: ElevatedButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Skip"),
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
