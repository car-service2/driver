import 'package:flutter/material.dart';

class DateRangeInput extends StatefulWidget {
  final DateTime? start;
  final DateTime? end;
  final Function(DateTime start, DateTime end) onChanged;

  const DateRangeInput({
    super.key,
    required this.onChanged,
    this.start,
    this.end,
  });

  @override
  State<StatefulWidget> createState() => _DateRangeInputState();
}

class _DateRangeInputState extends State<DateRangeInput> {
  late DateTimeRange selectedDateRange;

  @override
  void initState() {
    super.initState();
    selectedDateRange = DateTimeRange(
      start: widget.start ??
          DateTime.utc(DateTime.now().year, DateTime.now().month, 1),
      end: widget.end ??
          DateTime.utc(DateTime.now().year, DateTime.now().month + 1)
              .subtract(const Duration(days: 1)),
    );
  }

  Future<void> _selectDateRange(BuildContext context) async {
    final DateTimeRange? picked = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
      initialDateRange: selectedDateRange,
    );

    if (picked != null && picked != selectedDateRange) {
      setState(() {
        selectedDateRange = picked;
      });
      widget.onChanged(selectedDateRange.start, selectedDateRange.end);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _selectDateRange(context);
      },
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: const Color(0xFFF6F6F6)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "From",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: const Color(0xFF0D0D0D).withOpacity(0.3)),
                  ),
                  const Spacer(),
                  Text(
                    '${selectedDateRange.start.toLocal()}'.split(' ')[0],
                    style: const TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: const Color(0xFFF6F6F6),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "To",
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: const Color(0xFF0D0D0D).withOpacity(0.3),
                    ),
                  ),
                  const Spacer(),
                  Text(
                    '${selectedDateRange.end.toLocal()}'.split(' ')[0],
                    style: const TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
