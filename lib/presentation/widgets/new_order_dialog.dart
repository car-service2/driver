import 'package:driver/domain/entities/order.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import 'button_widget.dart';
import 'outlined_button_widget.dart';

class NewOrderDialog extends StatelessWidget {
  static Future<T?> show<T>({
    required BuildContext context,
    required OrderEntity order,
    required VoidCallback onDecline,
    required VoidCallback onAccept,
  }) async {
    return await showDialog<T>(
      context: context,
      builder: (_) {
        return NewOrderDialog._(
          order: order,
          onDecline: onDecline,
          onAccept: onAccept,
        );
      },
    );
  }

  final OrderEntity order;
  final VoidCallback onDecline;
  final VoidCallback onAccept;

  const NewOrderDialog._({
    required this.onDecline,
    required this.onAccept,
    required this.order,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(20),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(34),
      ),
      child: FittedBox(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(22),
          decoration: BoxDecoration(
            color: const Color(0xFFF6F6F6),
            borderRadius: BorderRadius.circular(34),
          ),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(16.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(22),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Customer: ${order.customer}",
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              color: const Color(0xFF0D0D0D).withOpacity(0.4),
                            ),
                          ),
                          Text(
                            order.waypoints.firstOrNull?.address ?? "-",
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w700,
                              fontSize: 18,
                            ),
                            maxLines: 1,
                          ),
                          const SizedBox(height: 20),
                          Row(
                            children: [
                              SvgPicture.asset("assets/images/cash.svg"),
                              const SizedBox(width: 10),
                              Text(
                                "${order.paymentType} | ${order.tripPrice} USD",
                                style: GoogleFonts.montserrat(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14,
                                  color: const Color(0xFF0D0D0D),
                                ),
                              ),
                              const Spacer(),
                              SvgPicture.asset("assets/images/map.svg"),
                              const SizedBox(width: 10),
                              Text(
                                "3,5 km",
                                style: GoogleFonts.montserrat(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14,
                                  color: const Color(0xFF0D0D0D),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 20),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Date & time",
                                style: GoogleFonts.montserrat(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12,
                                  color:
                                      const Color(0xFF0D0D0D).withOpacity(0.4),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 4,
                                  horizontal: 8,
                                ),
                                decoration: BoxDecoration(
                                  color: const Color(0xFFF6F6F6),
                                  borderRadius: BorderRadius.circular(6),
                                ),
                                child: Text(
                                  order.createdAt,
                                  style: GoogleFonts.montserrat(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14,
                                    color: const Color(0xFF0D0D0D),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 20),
                          if (order.desciption.isNotEmpty) ...[
                            const SizedBox(height: 5),
                            Text(
                              "Description",
                              style: GoogleFonts.montserrat(
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: const Color(0xFF0D0D0D).withOpacity(0.4),
                              ),
                            ),
                            const SizedBox(height: 4),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 4,
                                horizontal: 8,
                              ),
                              decoration: BoxDecoration(
                                color: const Color(0xFFF6F6F6),
                                borderRadius: BorderRadius.circular(6),
                              ),
                              child: Text(
                                order.desciption,
                                style: GoogleFonts.montserrat(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: const Color(0xFF0D0D0D),
                                ),
                              ),
                            ),
                          ]
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Clipboard.setData(
                          ClipboardData(text: order.waypoints.first.address),
                        ).then(
                          (value) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text("Address copied"),
                              ),
                            );
                          },
                        );
                      },
                      child: const Icon(Icons.copy),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 14),
              ButtonWidget(
                title: "Accept",
                background: const Color(0xFF00AE74),
                onPressed: onAccept,
                loading: false,
              ),
              const SizedBox(height: 14),
              OutlinedButtonWidget(
                title: "Decline",
                background: Colors.black,
                onPressed: onDecline,
                loading: false,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
