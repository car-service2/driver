import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../theme/app_theme.dart';

class TextInput extends StatefulWidget {
  final TextEditingController controller;
  final String hint;
  final bool obscure;

  const TextInput({
    super.key,
    required this.controller,
    required this.hint,
    this.obscure = false,
  });

  @override
  State<TextInput> createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  bool obscureText = false;

  OutlineInputBorder get border => OutlineInputBorder(
        borderRadius: BorderRadius.circular(50.0),
        borderSide: const BorderSide(color: Colors.white, width: 0),
      );

  @override
  void initState() {
    obscureText = widget.obscure;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      obscureText: obscureText,
      decoration: InputDecoration(
        suffixIconColor: AppColors.background,
        suffixIcon: widget.obscure
            ? GestureDetector(
                onTap: () => setState(() => obscureText = !obscureText),
                child: Container(
                  margin: const EdgeInsets.symmetric(
                    vertical: 15,
                    horizontal: 10,
                  ),
                  child: Icon(
                    obscureText ? Icons.visibility : Icons.visibility_off,
                  ),
                ),
              )
            : null,
        hintText: widget.hint,
        contentPadding: const EdgeInsets.all(20),
        hintStyle: GoogleFonts.montserrat(
          fontWeight: FontWeight.w500,
          color: const Color(0xFF979797),
        ),
        border: border,
        errorBorder: border,
        focusedBorder: border,
        focusedErrorBorder: border,
        enabledBorder: border,
        disabledBorder: border,
        enabled: true,
        filled: true,
        fillColor: Colors.white,
      ),
    );
  }
}
