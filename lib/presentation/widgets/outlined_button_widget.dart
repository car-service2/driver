import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OutlinedButtonWidget extends StatelessWidget {
  final String title;
  final VoidCallback? onPressed;
  final bool loading;
  final Color? background;

  const OutlinedButtonWidget({
    super.key,
    required this.title,
    required this.onPressed,
    required this.loading,
    this.background,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: !loading ? onPressed : null,
      style: OutlinedButton.styleFrom(
        minimumSize: const Size.fromHeight(52),
        side: BorderSide(
          color: background ?? Colors.black,
        ),
        surfaceTintColor: background,
        shape: const StadiumBorder(),
        foregroundColor: background,
        textStyle: GoogleFonts.inter(
          fontWeight: FontWeight.w500,
          fontSize: 16,
        ),
        elevation: 0.0,
      ),
      child: Stack(
        children: [
          Center(
            child: Text(
              title,
              style: GoogleFonts.montserrat(
                fontSize: 16,
                height: 19 / 16,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          if (loading)
            const Align(
              alignment: Alignment.centerRight,
              child: SizedBox.square(
                dimension: 20,
                child: CircularProgressIndicator(
                  color: Colors.white,
                  strokeWidth: 1.5,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
