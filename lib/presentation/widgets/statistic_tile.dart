import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StatisticTileWidget extends StatelessWidget {
  final String title;
  final String value;

  const StatisticTileWidget({
    super.key,
    required this.title,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: GoogleFonts.montserrat(
            fontWeight: FontWeight.w400,
            fontSize: 14,
          ),
          textAlign: TextAlign.start,
        ),
        Text(
          value,
          style: GoogleFonts.montserrat(
            fontWeight: FontWeight.w600,
            fontSize: 14,
          ),
          textAlign: TextAlign.end,
        ),
      ],
    );
  }
}
