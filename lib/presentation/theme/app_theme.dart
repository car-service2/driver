import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  static final ThemeData themeData = ThemeData(
    scaffoldBackgroundColor: AppColors.background,
    useMaterial3: false,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        maximumSize: const Size.fromHeight(60),
        backgroundColor: Colors.black,
        textStyle: GoogleFonts.montserrat(
          fontSize: 16,
          height: 19 / 16,
          fontWeight: FontWeight.w500,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
      ),
    ),
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: Colors.black,
    ),
    colorScheme: ColorScheme.fromSwatch(),
    appBarTheme: AppBarTheme(
      color: AppColors.background,
      elevation: 0.0,
      titleTextStyle: GoogleFonts.montserrat(
        fontWeight: FontWeight.w700,
        fontSize: 24,
        color: Colors.black,
      ),
    ),
    drawerTheme: DrawerThemeData(
      backgroundColor: AppColors.background,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      selectedLabelStyle: GoogleFonts.montserrat(
        fontSize: 12,
        fontWeight: FontWeight.w600,
      ),
      selectedItemColor: AppColors.background,
    ),
  );
}

class AppColors {
  static Color background = const Color(0xFF57BF9C);
}
