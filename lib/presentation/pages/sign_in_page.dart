import 'package:driver/presentation/blocs/login/login_cubit.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

import '../widgets/button_widget.dart';
import '../widgets/text_input.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final TextEditingController loginController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool agree = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        bottom: false,
        child: KeyboardDismissOnTap(
          dismissOnCapturedTaps: true,
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(horizontal: 36),
            child: Column(
              children: [
                const Spacer(),
                const SizedBox(height: 22),
                Text(
                  "Sign In",
                  style: GoogleFonts.montserrat(
                    fontSize: 36,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const Spacer(
                  flex: 2,
                ),
                TextInput(
                  controller: loginController,
                  hint: "Login",
                ),
                const SizedBox(height: 20),
                TextInput(
                  controller: passwordController,
                  hint: "Password",
                  obscure: true,
                ),
                const SizedBox(height: 20),
                PrivacyPolicyWidget(
                  onChanged: (bool? value) {
                    setState(() => agree = value == true);
                  },
                ),
                const Spacer(flex: 3),
                BlocConsumer<LoginCubit, LoginState>(
                  listener: (context, state) {
                    if (state is LoginFailure) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(state.message),
                        ),
                      );
                    }
                  },
                  builder: (context, state) {
                    return ButtonWidget(
                      title: "Sign in",
                      onPressed: agree
                          ? () {
                              BlocProvider.of<LoginCubit>(context).login(
                                login: loginController.text,
                                password: passwordController.text,
                              );
                            }
                          : null,
                      loading: state is LoginLoading,
                    );
                  },
                ),
                const Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PrivacyPolicyWidget extends StatefulWidget {
  final Function(bool? value) onChanged;

  const PrivacyPolicyWidget({super.key, required this.onChanged});

  @override
  State<PrivacyPolicyWidget> createState() => _PrivacyPolicyWidgetState();
}

class _PrivacyPolicyWidgetState extends State<PrivacyPolicyWidget> {
  bool _isChecked = false;

  void _toggleCheckbox(bool? value) {
    setState(() {
      _isChecked = value!;
    });
    widget.onChanged(value);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Checkbox(
              value: _isChecked,
              onChanged: _toggleCheckbox,
            ),
            Expanded(
              child: RichText(
                text: TextSpan(
                  text: 'I agree to the ',
                  style: const TextStyle(color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Privacy Policy',
                      style: const TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          launchUrl(
                              Uri.parse('http://arideinc.com/driver/privacy-policy'));
                        },
                    ),
                    const TextSpan(
                      text:
                          ' of A-Ride Driver and authorize the use of my data in accordance with it.',
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
