import 'package:driver/presentation/blocs/permissions/permissions_cubit.dart';
import 'package:driver/presentation/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GeolocationPage extends StatelessWidget {
  const GeolocationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Welcome to A-Ride Driver",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(fontWeight: FontWeight.w700, color: Colors.white),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(
          top: 24,
          right: 24,
          left: 24,
        ),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Spacer(),
              Row(
                children: [
                  Icon(
                    Icons.location_on_sharp,
                    size: 40,
                    color: AppColors.background,
                  ),
                  const SizedBox(width: 20),
                  Text(
                    "Location Access",
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(fontWeight: FontWeight.w700),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              titleSubtitle(
                title: 'Always Up-to-Date with Orders:',
                subtitle:
                    "Continuous access to your location allows the app to automatically offer you nearby orders, even when it's not active.",
                context: context,
              ),
              titleSubtitle(
                title: 'Seamless Navigation:',
                subtitle:
                    'Helps in routing and planning trips, saving your time.',
                context: context,
              ),
              titleSubtitle(
                title: 'How to Enable?',
                subtitle:
                    "Choose \"Always Allow\" when prompted for location access, to ensure you don't miss out on profitable orders.",
                context: context,
              ),
              titleSubtitle(
                title: 'Your Privacy:',
                subtitle:
                    "Your data is used solely for managing orders. Access settings can be changed at any time.",
                context: context,
              ),
              const Spacer(),
              const SizedBox(height: 20),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 0.0,
                    minimumSize: const Size.fromHeight(54),
                    backgroundColor: AppColors.background),
                onPressed: () {
                  BlocProvider.of<PermissionsCubit>(context)
                      .requestGeolocation()
                      .then((value) {
                    Navigator.pop(context);
                    BlocProvider.of<PermissionsCubit>(context)
                        .checkPermissions();
                  });
                },
                child: const Text("Next"),
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  Widget titleSubtitle({
    required String title,
    required String subtitle,
    required BuildContext context,
  }) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.titleLarge?.copyWith(
                fontWeight: FontWeight.w700,
              ),
          textAlign: TextAlign.start,
        ),
        const SizedBox(height: 10),
        Text(
          subtitle,
          style: Theme.of(context).textTheme.titleMedium,
          textAlign: TextAlign.justify,
        ),
        const SizedBox(height: 10),
      ],
    );
  }
}
