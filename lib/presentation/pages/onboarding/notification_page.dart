import 'package:driver/presentation/blocs/permissions/permissions_cubit.dart';
import 'package:driver/presentation/pages/onboarding/geolocation_page.dart';
import 'package:driver/presentation/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Welcome to A-Ride Driver",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(fontWeight: FontWeight.w700, color: Colors.white),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(
          top: 24,
          right: 24,
          left: 24,
        ),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Spacer(),
              Row(
                children: [
                  Icon(
                    Icons.notifications_active,
                    size: 40,
                    color: AppColors.background,
                  ),
                  const SizedBox(width: 20),
                  Text(
                    "Notification",
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(fontWeight: FontWeight.w700),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              titleSubtitle(
                title: 'Stay Updated, Respond Quickly:',
                subtitle:
                    "Enable notifications to get real-time ride requests and updates. This is key to responding swiftly and managing your rides effectively.",
                context: context,
              ),
              titleSubtitle(
                title: 'Easy Setup:',
                subtitle:
                    'Tap "Next" and choose "Allow" in the pop-up to start receiving important alerts and updates.',
                context: context,
              ),
              titleSubtitle(
                title: 'Your Control:',
                subtitle:
                    'our privacy is important. Adjust notification settings anytime in the app or device settings.',
                context: context,
              ),
              const Spacer(),
              const SizedBox(height: 20),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 0.0,
                    minimumSize: const Size.fromHeight(54),
                    backgroundColor: AppColors.background),
                onPressed: () {
                  BlocProvider.of<PermissionsCubit>(context)
                      .requestNotification()
                      .then((value) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => const GeolocationPage(),
                      ),
                    );
                  });
                },
                child: const Text("Next"),
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  Widget titleSubtitle({
    required String title,
    required String subtitle,
    required BuildContext context,
  }) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.titleLarge?.copyWith(
                fontWeight: FontWeight.w700,
              ),
          textAlign: TextAlign.start,
        ),
        const SizedBox(height: 10),
        Text(
          subtitle,
          style: Theme.of(context).textTheme.titleMedium,
          textAlign: TextAlign.justify,
        ),
        const SizedBox(height: 10),
      ],
    );
  }
}
