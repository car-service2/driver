import 'package:app_settings/app_settings.dart';
import 'package:driver/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:driver/presentation/blocs/auth/auth_bloc.dart';
import 'package:driver/presentation/blocs/inline/inline_cubit.dart';
import 'package:driver/presentation/blocs/permissions/permissions_cubit.dart';
import 'package:driver/presentation/theme/app_theme.dart';
import 'package:driver/presentation/widgets/button_widget.dart';
import 'package:driver/presentation/widgets/outlined_button_widget.dart';
import 'package:driver/presentation/widgets/set_arrive_time_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, authState) {
        if (authState is AuthAuthenticated) {
          return SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Row(
                    children: [
                      SizedBox.square(
                        dimension: 50,
                        child: authState.user.avatar == null
                            ? const Icon(
                                Icons.account_circle,
                                size: 50,
                                color: Colors.grey,
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.network(
                                  authState.user.avatar!,
                                  fit: BoxFit.cover,
                                ),
                              ),
                      ),
                      const SizedBox(width: 15),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              authState.user.name,
                              style: GoogleFonts.montserrat(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                            ),
                            BlocBuilder<InlineCubit, InlineState>(
                              buildWhen: (prev, next) => next is InlineChanged,
                              builder: (context, state) {
                                if (state is InlineChanged) {
                                  return Text(
                                    state.inline ? "Online" : "Offline",
                                    style: GoogleFonts.montserrat(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: state.inline
                                          ? const Color(0xFF57BF9C)
                                          : const Color(0xFFEE4646),
                                    ),
                                  );
                                }

                                return const Text("");
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 32,
                        width: 60,
                        child: BlocBuilder<InlineCubit, InlineState>(
                          buildWhen: (prev, next) => next is InlineChanged,
                          builder: (context, state) {
                            if (state is InlineChanged) {
                              return CupertinoSwitch(
                                value: state.inline,
                                activeColor: const Color(0xFF57BF9C),
                                trackColor: const Color(0xFFDFDFDF),
                                onChanged: (bool value) {
                                  BlocProvider.of<InlineCubit>(context)
                                      .change();
                                },
                              );
                            }

                            if (state is InlineChanging) {
                              return Stack(
                                children: [
                                  Center(
                                    child: SizedBox.square(
                                      dimension: 20,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 1.5,
                                        color: AppColors.background,
                                      ),
                                    ),
                                  ),
                                  const CupertinoSwitch(
                                    value: false,
                                    activeColor: Color(0xFF57BF9C),
                                    trackColor: Color(0xFFDFDFDF),
                                    onChanged: null,
                                  ),
                                ],
                              );
                            }

                            return const SizedBox();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                BlocBuilder<PermissionsCubit, PermissionsState>(
                  bloc: BlocProvider.of<PermissionsCubit>(context)
                    ..checkPermissions(),
                  builder: (context, state) {
                    if (state is PermissionsLoaded) {
                      if (!state.granted) {
                        return Center(
                          child: Container(
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            padding: const EdgeInsets.all(16),
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Column(
                                    children: [
                                      if (!state.allowedGeolocation)
                                        const Row(
                                          children: [
                                            Icon(
                                              Icons.location_on_rounded,
                                              color: Colors.grey,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: Text(
                                                "Please enable geolocation so the app can send you nearby orders.",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ),
                                          ],
                                        ),
                                      const SizedBox(height: 5),
                                      if (state.allowedGeolocation &&
                                          !state.locationAlwaysIsGranted)
                                        const Row(
                                          children: [
                                            Icon(
                                              Icons.location_on_rounded,
                                              color: Colors.grey,
                                            ),
                                            SizedBox(width: 10),
                                            Expanded(
                                              child: Text(
                                                "Please set geolocation to \"Always\" mode, allowing the app to send nearby orders even when closed.",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ),
                                          ],
                                        ),
                                      const SizedBox(height: 10),
                                      if (!state.allowedNotification)
                                        const Row(
                                          children: [
                                            Icon(
                                              Icons.notifications_active,
                                              color: Colors.grey,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: Text(
                                                "Please enable push notifications to receive orders when your app is closed or inactive.",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      const SizedBox(height: 10),
                                      const Text('then tap "Enabled" button'),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 20),
                                SizedBox(
                                  height: 40,
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: OutlinedButtonWidget(
                                          title: "Settings",
                                          background: Colors.green,
                                          onPressed: () {
                                            AppSettings.openAppSettings();
                                          },
                                          loading: false,
                                        ),
                                      ),
                                      const SizedBox(width: 20),
                                      Expanded(
                                        child: ButtonWidget(
                                          onPressed: () {
                                            BlocProvider.of<PermissionsCubit>(
                                                    context)
                                                .checkPermissions();
                                          },
                                          title: 'Enabled',
                                          loading: false,
                                          background: Colors.green,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                      return const SizedBox(height: 10);
                    }
                    return const SizedBox(height: 10);
                  },
                ),
                BlocBuilder<InlineCubit, InlineState>(
                  buildWhen: (prev, next) => next is InlineChanged,
                  builder: (context, inlineState) {
                    final active =
                        inlineState is InlineChanged && inlineState.inline;

                    return Container(
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              SvgPicture.asset("assets/images/location.svg"),
                              const SizedBox(width: 10),
                              Text(
                                "Choose were you are",
                                style: GoogleFonts.montserrat(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                ),
                              )
                            ],
                          ),
                          const Divider(thickness: 1),
                          BlocConsumer<AddressesCubit, AddressesState>(
                            bloc: context.read<AddressesCubit>()..load(),
                            listener: (context, state) {
                              if (state is AddressesFailure) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text(state.message)),
                                );
                              }
                            },
                            builder: (context, state) {
                              if (state is AddressesLoaded) {
                                return Column(
                                  children: state.addresses
                                      .map(
                                        (address) => CheckBoxWidget(
                                          title: address.name,
                                          value:
                                              address == state.selectedAddress,
                                          onChanged: (value) {
                                            SetArriveTimeDialog.show(
                                                    context, address)
                                                .then((datetime) {
                                              BlocProvider.of<AddressesCubit>(
                                                      context)
                                                  .select(address, datetime);
                                            });
                                          },
                                          active: active,
                                        ),
                                      )
                                      .toList(),
                                );
                              }

                              return SizedBox.square(
                                dimension: 20,
                                child: CircularProgressIndicator(
                                  strokeWidth: 1.5,
                                  color: AppColors.background,
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ],
            ),
          );
        }

        return Container();
      },
    );
  }
}

class CheckBoxWidget extends StatelessWidget {
  final String title;
  final bool value;
  final Function(bool? value) onChanged;
  final bool active;

  const CheckBoxWidget({
    super.key,
    required this.title,
    required this.value,
    required this.onChanged,
    required this.active,
  });

  Color get _color {
    if (!active) {
      return const Color(0xFFF1F1F1);
    }

    if (value) {
      return const Color(0xFF00A19A);
    } else {
      return Colors.white;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: GestureDetector(
        onTap: active ? () => onChanged(!value) : null,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: GoogleFonts.montserrat(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            Container(
              height: 20,
              width: 20,
              decoration: BoxDecoration(
                color: _color,
                border: Border.all(
                  color: value && active
                      ? const Color(0xFF00A19A)
                      : const Color(0xFFCCCCD6),
                ),
                borderRadius: BorderRadius.circular(4),
              ),
              child: value && active
                  ? Center(
                      child: SvgPicture.asset("assets/images/check.svg"),
                    )
                  : null,
            )
          ],
        ),
      ),
    );
  }
}
