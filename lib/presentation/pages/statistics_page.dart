import 'package:driver/presentation/widgets/failure_widget.dart';
import 'package:driver/utils/num_extention.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import '../blocs/statistics/statistics_cubit.dart';
import '../widgets/date_range_input.dart';
import '../widgets/statistic_tile.dart';

class StatisticsPage extends StatelessWidget {
  const StatisticsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          BlocBuilder<StatisticsDailyCubit, StatisticsState>(
            bloc: BlocProvider.of<StatisticsDailyCubit>(context)..daily(),
            builder: (context, state) {
              final statistic =
                  state is StatisticsLoaded ? state.statistic : null;

              if (state is StatisticsFailure) {
                return FailureWidget(
                  message: state.message,
                  onPressed: () {},
                );
              }

              return Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(24),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(22),
                ),
                child: Column(
                  children: [
                    Text(
                      "Today",
                      style: GoogleFonts.montserrat(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 5),
                    Text(
                      "${statistic?.total.formatWithTwoDecimals() ?? '--'} USD",
                      style: GoogleFonts.montserrat(
                        fontWeight: FontWeight.w700,
                        fontSize: 28,
                      ),
                    ),
                    Text(
                      "${statistic?.count ?? '--'} orders",
                      style: GoogleFonts.montserrat(
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        color: const Color(0xFF0D0D0D).withOpacity(0.4),
                      ),
                    ),
                    const SizedBox(height: 10),
                    StatisticTileWidget(
                      title: "Cash",
                      value:
                          "${statistic?.cash.formatWithTwoDecimals() ?? '--'} USD",
                    ),
                    Divider(
                      color: const Color(0xFF0D0D0D).withOpacity(0.09),
                      thickness: 1,
                    ),
                    StatisticTileWidget(
                      title: "Credit card",
                      value:
                          "${statistic?.credit.formatWithTwoDecimals() ?? '--'} USD",
                    ),
                    Divider(
                      color: const Color(0xFF0D0D0D).withOpacity(0.09),
                      thickness: 1,
                    ),
                    StatisticTileWidget(
                      title: "Toll",
                      value:
                          "${statistic?.toll.formatWithTwoDecimals() ?? '--'} USD",
                    ),
                    Divider(
                      color: const Color(0xFF0D0D0D).withOpacity(0.09),
                      thickness: 1,
                    ),
                    StatisticTileWidget(
                      title: "Company percentage",
                      value:
                          "${statistic?.percentage.formatWithTwoDecimals() ?? '--'} USD",
                    ),
                  ],
                ),
              );
            },
          ),
          const SizedBox(height: 20),
          BlocBuilder<StatisticsCustomCubit, StatisticsState>(
            bloc: BlocProvider.of<StatisticsCustomCubit>(context)..load(),
            builder: (context, state) {
              final statistic =
                  state is StatisticsLoaded ? state.statistic : null;
              final DateTime? start =
                  state is StatisticsLoaded ? state.startDate : null;
              final DateTime? end =
                  state is StatisticsLoaded ? state.endDate : null;

              if (state is StatisticsFailure) {
                return FailureWidget(
                  message: state.message,
                  onPressed: () {
                    BlocProvider.of<StatisticsCustomCubit>(context).load();
                  },
                );
              }

              return Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(24),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(22),
                ),
                child: Column(
                  children: [
                    DateRangeInput(
                      onChanged: (DateTime start, DateTime end) {
                        BlocProvider.of<StatisticsCustomCubit>(context).custom(
                          startDate: start,
                          endDate: end,
                        );
                      },
                      start: start,
                      end: end,
                    ),
                    const SizedBox(height: 10),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: const Color(0xFFF6F6F6),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Column(
                        children: [
                          const SizedBox(height: 5),
                          Text(
                            "${statistic?.total.formatWithTwoDecimals() ?? '--'} USD",
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w700,
                              fontSize: 28,
                            ),
                          ),
                          Text(
                            "${statistic?.count ?? '--'} orders",
                            style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: const Color(0xFF0D0D0D).withOpacity(0.4),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 20),
                    StatisticTileWidget(
                      title: "Cash",
                      value:
                          "${statistic?.cash.formatWithTwoDecimals() ?? '--'} USD",
                    ),
                    Divider(
                      color: const Color(0xFF0D0D0D).withOpacity(0.09),
                      thickness: 1,
                    ),
                    StatisticTileWidget(
                      title: "Credit card",
                      value:
                          "${statistic?.credit.formatWithTwoDecimals() ?? '--'} USD",
                    ),
                    Divider(
                      color: const Color(0xFF0D0D0D).withOpacity(0.09),
                      thickness: 1,
                    ),
                    StatisticTileWidget(
                      title: "Toll",
                      value:
                          "${statistic?.toll.formatWithTwoDecimals() ?? '--'} USD",
                    ),
                    Divider(
                      color: const Color(0xFF0D0D0D).withOpacity(0.09),
                      thickness: 1,
                    ),
                    StatisticTileWidget(
                      title: "Company percentage",
                      value:
                          "${statistic?.percentage.formatWithTwoDecimals() ?? '--'} USD",
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
