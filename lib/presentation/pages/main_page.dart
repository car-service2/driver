import 'dart:async';
import 'dart:convert';

import 'package:driver/domain/entities/order.dart';
import 'package:driver/presentation/blocs/addresses/addresses_cubit.dart';
import 'package:driver/presentation/blocs/auth/auth_bloc.dart';
import 'package:driver/presentation/blocs/inline/inline_cubit.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../blocs/menu/menu_cubit.dart';
import '../blocs/new_order/new_order_bloc.dart';
import '../blocs/order_status/order_status_cubit.dart';
import '../blocs/orders/orders_cubit.dart';
import '../widgets/botton_nav_bar.dart';
import '../widgets/choose_address_bottom_sheet.dart';
import '../widgets/drawer.dart';
import '../widgets/new_order_dialog.dart';
import 'orders_page/orders_page.dart';
import 'orders_page/set_order_page.dart';
import 'settings_page.dart';
import 'statistics_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late final OrdersCubit _ordersCubit;
  late final InlineCubit _inlineCubit;
  late final AddressesCubit _addressesCubit;
  late Timer timer;
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  final streamController = StreamController<Map<String, dynamic>>();

  Future<void> fetchGeolocation() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.reload();
    final jsonString = prefs.getString('locationChanged');

    if (jsonString != null) {
      final jsonMap = json.decode(jsonString);
      if (jsonMap is Map<String, dynamic>) {
        streamController.add(jsonMap);
      } else {
        streamController.add({});
      }
    } else {
      streamController.add({});
    }
  }

  @override
  void initState() {
    debugPrint("MAIN PAGE INIT STATE");
    super.initState();

    if (kDebugMode) {
      timer = Timer.periodic(
        const Duration(seconds: 1),
        (_) => fetchGeolocation(),
      );
    }

    _ordersCubit = BlocProvider.of<OrdersCubit>(context);
    _inlineCubit = BlocProvider.of<InlineCubit>(context);
    _addressesCubit = BlocProvider.of<AddressesCubit>(context);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (BlocProvider.of<AuthBloc>(context).state is AuthAuthenticated) {
        var authState =
            BlocProvider.of<AuthBloc>(context).state as AuthAuthenticated;
        _inlineCubit.load(authState.user);
        _addressesCubit.load();
        _inlineCubit.stream.listen((state) {
          if (state is InlineChanged) {
            _addressesCubit.load();
          }
        });
      }
    });
  }

  @override
  void dispose() {
    if (kDebugMode) {
      timer.cancel();
      streamController.close();
    }
    super.dispose();
  }

  // Create a key
  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<InlineCubit, InlineState>(listener: _inlineListener),
        BlocListener<NewOrderBloc, NewOrderState>(listener: _newOrderListener),
      ],
      child: Scaffold(
        key: _key,
        drawer: const DrawerWidget(),
        appBar: AppBar(
          leading: IconButton(
            icon: SvgPicture.asset("assets/images/menu.svg"),
            onPressed: () {
              _key.currentState?.openDrawer();
            },
          ),
          title: BlocBuilder<MenuCubit, Menu>(
            builder: (context, state) => Text(state.appBarTitle),
          ),
          centerTitle: true,
          actions: [
            if (kDebugMode)
              StreamBuilder<Map<String, dynamic>?>(
                stream: streamController.stream,
                builder: (context, snapshot) {
                  return GestureDetector(
                    onTap: () {
                      SharedPreferences.getInstance()
                          .then((value) => value.remove("locationChanged"));
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "${snapshot.data?['latitude'] ?? '---'}",
                          style: const TextStyle(fontSize: 10),
                        ),
                        Text(
                          "${snapshot.data?['longitude'] ?? '---'}",
                          style: const TextStyle(fontSize: 10),
                        ),
                      ],
                    ),
                  );
                },
              ),
            BlocBuilder<MenuCubit, Menu>(
              builder: (context, state) {
                if (Menu.orders == state) {
                  return IconButton(
                    onPressed: () {
                      Navigator.push(context, SetOrderPage.route());
                    },
                    icon: const Icon(
                      Icons.add_circle_outlined,
                      size: 30,
                      color: Colors.black,
                    ),
                  );
                }

                return const SizedBox();
              },
            )
          ],
        ),
        body: BlocBuilder<MenuCubit, Menu>(
          builder: (context, state) {
            return switch (state) {
              Menu.orders => const OrdersPage(),
              Menu.settings => const SettingsPage(),
              Menu.statistics => const StatisticsPage(),
            };
          },
        ),
        bottomNavigationBar: const BottomNavBar(),
      ),
    );
  }

  void _newOrderListener(context, NewOrderState state) async {
    if (state is NewOrderReceived) {
      debugPrint("received new order! ${state.order.id}");
      await NewOrderDialog.show(
        context: context,
        order: state.order,
        onDecline: () {
          BlocProvider.of<OrderStatusCubit>(context)
              .decline(orderId: state.order.id)
              .then((_) {
            BlocProvider.of<OrdersCubit>(context)
                .applyFilter(OrderStatusEntity.all);
          });
          Navigator.pop(context);
        },
        onAccept: () {
          BlocProvider.of<OrderStatusCubit>(context)
              .accept(orderId: state.order.id)
              .then((_) {
            BlocProvider.of<OrdersCubit>(context)
                .applyFilter(OrderStatusEntity.accept);
          });
          Navigator.pop(context);
        },
      );
      _ordersCubit.refresh();
    }
  }

  void _inlineListener(context, InlineState state) async {
    if (state is InlineChanged && state.inline) {
      await ChooseAddressBottomSheet.show(
        context: context,
        showCurrentLocation: true,
      );
    }

    if (state is InlineFailure) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Error update status")),
      );
    }
  }
}
