import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../data/models/waypoint.dart';
import 'add_waypoint_page.dart';
import 'section.dart';

class SelectWaypointsWidget extends StatefulWidget {
  final List<Waypoint> waypoints;
  final void Function(List<Waypoint> waypoints) onChanged;

  const SelectWaypointsWidget({
    super.key,
    required this.waypoints,
    required this.onChanged,
  });

  @override
  State<SelectWaypointsWidget> createState() => _SelectWaypointsWidgetState();
}

class _SelectWaypointsWidgetState extends State<SelectWaypointsWidget> {
  List<Waypoint> waypoints = [];

  @override
  void initState() {
    waypoints = widget.waypoints;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Section(
      title: 'Waypoints',
      children: [
        ...waypoints.map((wp) {
          final iconPath = wp == waypoints.first
              ? "assets/icons/focus-3-line.svg"
              : "assets/icons/map-pin-2-line.svg";
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              children: [
                SvgPicture.asset(
                  iconPath,
                  width: 18,
                  height: 18,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: const Color(0xFFF6F6F6),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("Address: ${wp.address}"),
                        Text("Round trip: ${wp.rt.toString()}"),
                        Text("Passengers: ${wp.passengers.toString()}"),
                      ],
                    ),
                  ),
                ),
                IconButton(
                  icon: SvgPicture.asset(
                    "assets/icons/delete-bin-line.svg",
                    colorFilter: const ColorFilter.mode(
                      Colors.red,
                      BlendMode.srcIn,
                    ),
                  ),
                  onPressed: () {
                    setState(() => waypoints.remove(wp));
                    widget.onChanged(waypoints);
                  },
                ),
              ],
            ),
          );
        }).toList(),
        ElevatedButton(
          style: TextButton.styleFrom(
            elevation: 0.0,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            minimumSize: const Size.fromHeight(40),
            maximumSize: const Size.fromHeight(40),
            backgroundColor: const Color(0xFF57BF9C),
          ),
          onPressed: () async {
            final wp = await Navigator.push<Waypoint>(
              context,
              MaterialPageRoute(
                builder: (_) => const AddWaypointPage(),
              ),
            );

            if (wp != null) {
              waypoints.add(wp);
              setState(() {});
              widget.onChanged(waypoints);
            }
          },
          child: const Text("Add waypoint"),
        )
      ],
    );
  }
}
