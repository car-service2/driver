import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomerDataWidget extends StatefulWidget {
  final String? initialPrice;
  final String? initialPhone;
  final void Function(String value) onPriceChanged;
  final void Function(String value) onPhoneChanged;

  const CustomerDataWidget({
    super.key,
    required this.onPriceChanged,
    required this.onPhoneChanged,
    this.initialPrice,
    this.initialPhone,
  });

  @override
  State<CustomerDataWidget> createState() => _CustomerDataWidgetState();
}

class _CustomerDataWidgetState extends State<CustomerDataWidget> {
  final TextEditingController priceController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();

  @override
  void initState() {
    priceController.text = widget.initialPrice ?? '';
    phoneController.text = widget.initialPhone ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(
        vertical: 14,
        horizontal: 15,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(18),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Customer data",
            style: GoogleFonts.montserrat(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(height: 20),
          IconTextField(
            icon: SvgPicture.asset(
              "assets/icons/phone-line.svg",
              width: 20,
              height: 20,
            ),
            controller: phoneController,
            hint: 'Phone number',
            keyboardType: TextInputType.phone,
            onChanged: widget.onPhoneChanged,
          ),
          const SizedBox(height: 10),
          IconTextField(
            icon: SvgPicture.asset(
              "assets/icons/bank-card-line.svg",
              width: 20,
              height: 20,
            ),
            controller: priceController,
            hint: 'Price',
            keyboardType: TextInputType.number,
            onChanged: widget.onPriceChanged,
          ),
        ],
      ),
    );
  }
}

class IconTextField extends StatefulWidget {
  final Widget icon;
  final TextEditingController controller;
  final String hint;
  final TextInputType keyboardType;
  final Function(String value) onChanged;
  final Widget? suffixIcon;

  const IconTextField({
    super.key,
    required this.icon,
    required this.controller,
    required this.hint,
    required this.keyboardType,
    required this.onChanged,
    this.suffixIcon,
  });

  @override
  State<IconTextField> createState() => _IconTextFieldState();
}

class _IconTextFieldState extends State<IconTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        filled: true,
        fillColor: const Color(0xFFF6F6F6),
        prefixIcon: SizedBox.square(
          dimension: 20,
          child: Center(child: widget.icon),
        ),
        suffixIcon: widget.suffixIcon,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide.none,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(color: Colors.black),
        ),
        hintText: widget.hint,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 13,
          vertical: 16,
        ),
      ),
    );
  }
}
