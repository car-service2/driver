import 'package:driver/domain/entities/order.dart';
import 'package:driver/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:intl/intl.dart';

import '../../../data/models/waypoint.dart';
import '../../blocs/create_order/create_order_cubit.dart';
import '../../blocs/orders/orders_cubit.dart';
import '../../widgets/button_widget.dart';
import 'customer_data_widget.dart';
import 'description_widget.dart';
import 'select_address_widget.dart';
import 'set_date_widget.dart';

class SetOrderPage extends StatefulWidget {
  const SetOrderPage({super.key});

  @override
  State<SetOrderPage> createState() => _SetOrderPageState();

  static Route<Object> route() {
    return MaterialPageRoute(
      builder: (context) => BlocProvider(
        create: (context) => locator.get<CreateOrderCubit>(),
        child: const SetOrderPage(),
      ),
    );
  }
}

class _SetOrderPageState extends State<SetOrderPage> {
  List<Waypoint> waypoints = [];
  DateTime dateTime = DateTime.now().add(const Duration(minutes: 5));
  String phone = '';
  String price = '';
  String description = '';

  bool get validated =>
      waypoints.isNotEmpty && phone.isNotEmpty && price.isNotEmpty;

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Create order"),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(14),
          child: SafeArea(
            child: Column(
              children: [
                SelectWaypointsWidget(
                  waypoints: waypoints,
                  onChanged: (List<Waypoint> waypoints) {
                    setState(() => this.waypoints = waypoints);
                  },
                ),
                const SizedBox(height: 14),
                SetDateWidget(
                  dateTime: dateTime,
                  onChanged: (value) {
                    setState(() => dateTime = value);
                  },
                ),
                const SizedBox(height: 14),
                CustomerDataWidget(
                  initialPrice: price,
                  initialPhone: phone,
                  onPriceChanged: (String value) {
                    setState(() => price = value);
                  },
                  onPhoneChanged: (String value) {
                    setState(() => phone = value);
                  },
                ),
                const SizedBox(height: 14),
                DescriptionWidget(
                  initialDescription: description,
                  onChanged: (String value) {
                    setState(() => description = value);
                  },
                ),
                const SizedBox(height: 24),
                BlocConsumer<CreateOrderCubit, CreateOrderState>(
                  listener: (context, state) {
                    if (state is CreateOrderFailure) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text("Error saving order"),
                        ),
                      );
                    }
                    if (state is CreateOrderCreated) {
                      Navigator.pop(context, true);
                      BlocProvider.of<OrdersCubit>(context).refresh();
                    }
                  },
                  builder: (context, state) {
                    return ButtonWidget(
                      onPressed: validated ? onSave : null,
                      title: 'Create order',
                      loading: state is CreateOrderLoading,
                    );
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onSave() {
    var order = OrderEntity.fromDriver(
      waypoints: waypoints,
      date: DateFormat('yyyy-MM-dd HH:mm:ss').format(dateTime),
      phone: phone,
      price: double.tryParse(price) ?? 0.0,
      description: description,
    );
    BlocProvider.of<CreateOrderCubit>(context).create(order);
  }
}
