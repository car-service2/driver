import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../domain/entities/order.dart';
import '../../../blocs/orders/orders_cubit.dart';
import '../../../widgets/order_widget.dart';

class OrderList extends StatefulWidget {
  final OrderStatusEntity status;

  const OrderList({super.key, required this.status});

  @override
  State<OrderList> createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  final ScrollController _scrollController = ScrollController();

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.atEdge &&
        _scrollController.position.pixels != 0) {
      // Достигнут конец списка
      _loadData();
    }
  }

  Future<void> _loadData() async {
    if (!_isLoading) {
      // Предотвращаем повторную загрузку данных во время подгрузки
      setState(() => _isLoading = true);

      try {
        await BlocProvider.of<OrdersCubit>(context)
            .load(); // Загружаем данные из кубита
      } catch (e) {
        debugPrint(e.toString());
      }

      // Завершаем подгрузку
      setState(() => _isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrdersCubit, OrdersState>(
      builder: (context, state) {
        if (state is OrdersLoaded) {
          return SafeArea(
            child: NotificationListener<ScrollNotification>(
              onNotification: (notification) {
                // Добавляем обработчик прокрутки для определения конца списка
                if (!_isLoading &&
                    notification.metrics.pixels >=
                        notification.metrics.maxScrollExtent) {
                  _loadData();
                }
                return false;
              },
              child: ListView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                controller: _scrollController,
                itemCount:
                    _isLoading ? state.orders.length + 1 : state.orders.length,
                itemBuilder: (context, index) {
                  if (index < state.orders.length) {
                    // Отображаем элемент списка
                    final order = state.orders[index];
                    return OrderWidget(order: order);
                  } else {
                    // Достигли конца списка, показываем индикатор загрузки
                    return _isLoading
                        ? const Center(
                            child: SizedBox.square(
                              dimension: 25,
                              child: CircularProgressIndicator(
                                color: Colors.white,
                                strokeWidth: 1.5,
                              ),
                            ),
                          )
                        : Container();
                  }
                },
              ),
            ),
          );
        }

        return const SizedBox();
      },
    );
  }
}
