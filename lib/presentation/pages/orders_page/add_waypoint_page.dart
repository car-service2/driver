import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import '../../../data/models/waypoint.dart';
import '../../theme/app_theme.dart';
import '../../widgets/button_widget.dart';

class AddWaypointPage extends StatefulWidget {
  const AddWaypointPage({super.key});

  @override
  State<AddWaypointPage> createState() => _AddWaypointPageState();
}

class _AddWaypointPageState extends State<AddWaypointPage> {
  final apiKey = 'AIzaSyCPt9UfXRftNIlfVraTVOWSU3V0rOcVJFI';
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _rtController = TextEditingController();
  final TextEditingController _passengersController = TextEditingController();
  double _latitude = 0;
  double _longitude = 0;
  final Dio _dio = Dio();
  List<AddressData> _suggestions = [];
  bool _isAddressSelected = false;
  final FocusNode focusNode = FocusNode();

  void _getAddressSuggestions(String input) async {
    const endpoint =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json';

    try {
      final response = await _dio.get(
        endpoint,
        queryParameters: {
          'input': input,
          'key': apiKey,
        },
      );

      if (response.statusCode == 200) {
        setState(() {
          final predictions = response.data['predictions'];
          _suggestions = predictions.map<AddressData>((prediction) {
            return AddressData(
              description: prediction['description'] as String,
              placeId: prediction['place_id'] as String,
            );
          }).toList();
        });
      }
    } catch (e) {
      // Обработайте ошибку запроса, если необходимо
      debugPrint('Error fetching address suggestions: $e');
    }
  }

  Future<void> _selectAddress(AddressData addressData) async {
    const endpoint = 'https://maps.googleapis.com/maps/api/place/details/json';

    try {
      final response = await _dio.get(
        endpoint,
        queryParameters: {
          'place_id': addressData.placeId,
          'key': apiKey,
        },
      );

      if (response.statusCode == 200) {
        final location = response.data['result']['geometry']['location'];
        final latitude = location['lat'];
        final longitude = location['lng'];

        debugPrint('Latitude: $latitude, Longitude: $longitude');
        _latitude = latitude;
        _longitude = longitude;
        _addressController.text = addressData.description;
        setState(() => _isAddressSelected = true);
      }
    } catch (e) {
      debugPrint('Error fetching place details: $e');
    }
  }

  @override
  void initState() {
    focusNode.requestFocus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      dismissOnCapturedTaps: true,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Add waypoint'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              const SizedBox(height: 10),
              !_isAddressSelected
                  ? TextFormField(
                      focusNode: focusNode,
                      controller: _addressController,
                      enabled: !_isAddressSelected,
                      maxLines: 5,
                      minLines: 3,
                      onChanged: (input) {
                        if (input.isNotEmpty) {
                          _getAddressSuggestions(input);
                        } else {
                          setState(() {
                            _suggestions = [];
                          });
                        }
                      },
                      decoration: IDecoration.input(
                        hint: "enter the address",
                      ),
                    )
                  : Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 15),
                      decoration: BoxDecoration(
                        color: const Color(0xFFF6F6F6),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              _addressController.text,
                              style: const TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              _addressController.clear();
                              setState(() => _isAddressSelected = false);
                              focusNode.requestFocus();
                            },
                            child: const Icon(Icons.clear_outlined),
                          ),
                        ],
                      ),
                    ),
              const SizedBox(height: 10),
              !_isAddressSelected && _suggestions.isNotEmpty
                  ? Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 20),
                              child: Text("Select address"),
                            ),
                            Expanded(
                              child: ListView.separated(
                                itemCount: _suggestions.length,
                                itemBuilder: (context, index) {
                                  final addressData = _suggestions[index];

                                  return ListTile(
                                    onTap: () => _selectAddress(addressData),
                                    leading: Icon(
                                      Icons.location_on_outlined,
                                      color: AppColors.background,
                                    ),
                                    minLeadingWidth: 0,
                                    title: Text(addressData.description),
                                  );
                                },
                                separatorBuilder: (
                                  BuildContext context,
                                  int index,
                                ) {
                                  return const Divider();
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : const SizedBox(),
              const SizedBox(height: 10),
              if (_isAddressSelected)
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        controller: _rtController,
                        decoration: IDecoration.input(hint: "Round trip"),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: TextField(
                        controller: _passengersController,
                        decoration: IDecoration.input(hint: "Passengers"),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                  ],
                ),
              if (_isAddressSelected) const Spacer(),
              if (_isAddressSelected)
                ButtonWidget(
                  title: "Add waypoint",
                  onPressed: _isAddressSelected ? _onAddAddress : null,
                  loading: false,
                ),
              const SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }

  void _onAddAddress() {
    final wp = Waypoint(
      rt: int.tryParse(_rtController.text) ?? 0,
      passengers: int.tryParse(_passengersController.text) ?? 0,
      address: _addressController.text,
      latitude: _latitude,
      longitude: _longitude,
    );
    Navigator.pop<Waypoint>(context, wp);
  }
}

class AddressData {
  final String description;
  final String placeId;

  AddressData({required this.description, required this.placeId});
}

class IDecoration {
  static InputDecoration input({required String hint}) {
    return InputDecoration(
      filled: true,
      fillColor: const Color(0xFFF6F6F6),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide.none,
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: const BorderSide(color: Colors.black),
      ),
      hintText: hint,
      contentPadding: const EdgeInsets.symmetric(
        horizontal: 13,
        vertical: 16,
      ),
    );
  }

  static InputDecoration search({
    required String hint,
    required VoidCallback onClear,
  }) {
    return InputDecoration(
      filled: true,
      fillColor: const Color(0xFFF6F6F6),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide.none,
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: const BorderSide(color: Colors.black),
      ),
      hintText: hint,
      contentPadding: const EdgeInsets.symmetric(
        horizontal: 13,
        vertical: 16,
      ),
      prefixIcon: const Icon(Icons.location_on),
      suffixIcon: GestureDetector(
        onTap: onClear,
        child: const Icon(Icons.close),
      ),
    );
  }
}
