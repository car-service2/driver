import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/order.dart';
import '../../blocs/orders/orders_cubit.dart';
import '../../widgets/status_tab_bar_button.dart';
import 'widgets/order_list.dart';

class OrdersPage extends StatefulWidget {
  const OrdersPage({super.key});

  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> with TickerProviderStateMixin {
  late TabController _tabController;
  late OrdersCubit ordersCubit;

  @override
  void initState() {
    super.initState();
    ordersCubit = BlocProvider.of<OrdersCubit>(context)..refresh();
    _tabController = TabController(
      length: OrderStatusEntity.values.length,
      vsync: this,
      initialIndex: ordersCubit.state.currentStatus.index,
    );
    _tabController.addListener(() {
      int pageIndex = _tabController.index;
      if (ordersCubit.state.currentStatus.index != pageIndex) {
        ordersCubit.applyFilter(OrderStatusEntity.values.elementAt(pageIndex));
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<OrdersCubit, OrdersState>(
      listenWhen: (prev, current) =>
          prev.currentStatus.index != current.currentStatus.index,
      listener: (context, state) {
        _tabController.index = state.currentStatus.index;
      },
      builder: (context, state) {
        return Column(
          children: [
            /// tabs
            TabBar(
              controller: _tabController,
              isScrollable: true,
              padding: EdgeInsets.zero,
              indicatorPadding: EdgeInsets.zero,
              indicator: const UnderlineTabIndicator(
                borderSide: BorderSide.none,
              ),
              labelPadding: const EdgeInsets.symmetric(horizontal: 5),
              tabs: OrderStatusEntity.values
                  .map(
                    (status) => StatusTabButton(
                      selected: status == state.currentStatus,
                      onPressed: () => ordersCubit.applyFilter(status),
                      title: status.toString(),
                    ),
                  )
                  .toList(),
            ),
            const SizedBox(height: 20),

            /// order list
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: OrderStatusEntity.values
                    .map((status) => OrderList(status: status))
                    .toList(),
              ),
            ),
          ],
        );
      },
    );
  }
}
