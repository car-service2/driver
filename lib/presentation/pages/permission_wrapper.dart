import 'package:driver/presentation/pages/onboarding/notification_page.dart';
import 'package:driver/presentation/widgets/loader_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/permissions/permissions_cubit.dart';

class PermissionsWrapper extends StatefulWidget {
  final Widget child;

  const PermissionsWrapper({
    super.key,
    required this.child,
  });

  @override
  State<PermissionsWrapper> createState() => _PermissionsWrapperState();
}

class _PermissionsWrapperState extends State<PermissionsWrapper>
    with WidgetsBindingObserver {
  late PermissionsCubit permissionsCubit;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    permissionsCubit = BlocProvider.of<PermissionsCubit>(context);
    permissionsCubit.start();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      debugPrint("AppLifecycle resumed check permissions");
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PermissionsCubit, PermissionsState>(
      builder: (context, state) {
        if (state is PermissionsLoaded) {
          if (state.isRequested) {
            return widget.child;
          }

          return const NotificationPage();
        }

        return const Scaffold(
          body: Center(child: LoaderWidget()),
        );
      },
    );
  }
}
