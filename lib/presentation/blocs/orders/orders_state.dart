part of 'orders_cubit.dart';

abstract class OrdersState {
  OrderStatusEntity get currentStatus;
}

class OrdersInitial extends OrdersState {
  final OrderStatusEntity _status;

  OrdersInitial(this._status);

  @override
  OrderStatusEntity get currentStatus => _status;
}

class OrdersLoading extends OrdersState {
  final OrderStatusEntity _status;

  OrdersLoading(this._status);

  @override
  OrderStatusEntity get currentStatus => _status;
}

class OrdersFailure extends OrdersState {
  final String message;
  final OrderStatusEntity _status;

  OrdersFailure(this.message, this._status);

  @override
  OrderStatusEntity get currentStatus => _status;
}

class OrdersLoaded extends OrdersState {
  final List<OrderEntity> orders;
  final OrderStatusEntity orderStatus;
  final bool allLoaded;

  OrdersLoaded(
    this.orders,
    this.orderStatus,
    this.allLoaded,
  );

  @override
  OrderStatusEntity get currentStatus => orderStatus;
}
