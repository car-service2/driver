import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/entities/order.dart';
import '../../../domain/usecases/order_usecase.dart';

part 'orders_state.dart';

class OrdersCubit extends Cubit<OrdersState> {
  final OrderUseCase _ordersUseCase;
  OrderStatusEntity _orderStatus = OrderStatusEntity.all;
  var _page = 1;
  final _perPage = 10;

  OrdersCubit(this._ordersUseCase)
      : super(OrdersInitial(OrderStatusEntity.all));

  Future<void> refresh() async {
    emit(OrdersInitial(_orderStatus));
    await load();
  }

  Future<void> load() async {
    if (state is OrdersLoaded && (state as OrdersLoaded).allLoaded) {
      return;
    }

    if (state is! OrdersLoaded) {
      _page = 1;
    }

    // try {
      debugPrint("$_page | $_perPage | $_orderStatus");
      final orders = await _ordersUseCase.getOrders(
        status: _orderStatus.toQueryParam(),
        page: _page,
        perPage: _perPage,
      );

      bool allLoaded = _perPage > orders.length;

      OrdersLoaded ordersLoadedState = OrdersLoaded(
        orders,
        _orderStatus,
        allLoaded,
      );
      if (state is OrdersLoaded) {
        ordersLoadedState = OrdersLoaded(
          [
            ...(state as OrdersLoaded).orders,
            ...orders,
          ],
          _orderStatus,
          allLoaded,
        );
      }
      emit(ordersLoadedState);
      _page++;
    // } catch (e) {
    //   _page = 1;
    //   debugPrint(e.toString());
    //   emit(
    //     OrdersFailure(e.toString(), _orderStatus),
    //   );
    // }
  }

  Future<void> applyFilter(OrderStatusEntity status) async {
    _orderStatus = status;
    await refresh();
  }
}
