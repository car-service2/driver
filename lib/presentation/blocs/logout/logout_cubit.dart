import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/usecases/auth_usecase.dart';
import '../auth/auth_bloc.dart';

part 'logout_state.dart';

class LogoutCubit extends Cubit<LogoutState> {
  final AuthUseCase _authUseCase;
  final AuthBloc _authBloc;

  LogoutCubit(this._authUseCase, this._authBloc) : super(LogoutInitial());

  Future<void> logout() async {
    emit(LogoutLoading());

    try {
      await _authUseCase.logOut();
      emit(LogoutSuccess());
      _authBloc.add(AuthCheckAuthenticationEvent());
    } catch (error) {
      debugPrint(error.toString());
      emit(LogoutFailure(error.toString()));
    }
  }
}
