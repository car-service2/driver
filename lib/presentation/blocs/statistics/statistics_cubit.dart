import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../domain/entities/statistic.dart';
import '../../../domain/usecases/statistics_usecase.dart';
import '../../../utils/date_time_extention.dart';

part 'statistics_state.dart';

class StatisticsDailyCubit extends Cubit<StatisticsState> {
  final StatisticsUseCase _statisticsUseCase;

  StatisticsDailyCubit(this._statisticsUseCase) : super(StatisticsInitial());

  Future<void> daily() async {
    try {
      final statistic = await _statisticsUseCase.daily();
      emit(StatisticsLoaded(
        statistic: statistic,
        startDate: DateTime.now(),
        endDate: DateTime.now(),
      ));
    } catch (e) {
      emit(StatisticsFailure(e.toString()));
    }
  }
}

class StatisticsCustomCubit extends Cubit<StatisticsState> {
  final StatisticsUseCase _statisticsUseCase;
  late DateTime _startDate;
  late DateTime _endDate;

  StatisticsCustomCubit(this._statisticsUseCase) : super(StatisticsInitial()) {
    _startDate = DateTime.utc(DateTime.now().year, DateTime.now().month, 1);
    _endDate = DateTime.utc(DateTime.now().year, DateTime.now().month + 1)
        .subtract(const Duration(days: 1));
  }

  Future<void> custom({
    required DateTime startDate,
    required DateTime endDate,
  }) async {
    _startDate = startDate;
    _endDate = endDate;
    await load();
  }

  Future<void> load() async {
    try {
      final StatisticEntity statistic = await _statisticsUseCase.custom(
        startDate: _startDate.toYMD(),
        endDate: _endDate.toYMD(),
      );
      emit(StatisticsLoaded(
        statistic: statistic,
        startDate: _startDate,
        endDate: _endDate,
      ));
    } catch (e) {
      emit(StatisticsFailure(e.toString()));
    }
  }
}
