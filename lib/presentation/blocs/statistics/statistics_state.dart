part of 'statistics_cubit.dart';

@immutable
abstract class StatisticsState {}

class StatisticsInitial extends StatisticsState {}

class StatisticsLoading extends StatisticsState {}

class StatisticsLoaded extends StatisticsState {
  final StatisticEntity statistic;
  final DateTime startDate;
  final DateTime endDate;

  StatisticsLoaded({
    required this.statistic,
    required this.startDate,
    required this.endDate,
  });
}

class StatisticsFailure extends StatisticsState {
  final String message;

  StatisticsFailure(this.message);
}
