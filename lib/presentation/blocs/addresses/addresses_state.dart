part of 'addresses_cubit.dart';

@immutable
abstract class AddressesState {}

class AddressesInitial extends AddressesState {}

class AddressesLoading extends AddressesState {}

class AddressesLoaded extends AddressesState {
  final List<AddressEntity> addresses;
  final AddressEntity? selectedAddress;

  AddressesLoaded({
    required this.addresses,
    this.selectedAddress,
  });
}

class AddressesFailure extends AddressesState {
  final String message;

  AddressesFailure({required this.message});
}
