import 'package:bloc/bloc.dart';
import 'package:driver/domain/repositories/auth_repository.dart';
import 'package:driver/service_locator.dart';
import 'package:meta/meta.dart';

import '../../../domain/entities/address.dart';
import '../../../domain/usecases/settings_usecase.dart';

part 'addresses_state.dart';

class AddressesCubit extends Cubit<AddressesState> {
  final SettingsUseCase _settingsUseCase;
  AddressEntity? selectedAddress;

  AddressesCubit(
    this._settingsUseCase,
  ) : super(AddressesInitial());

  Future<void> load() async {
    emit(AddressesLoading());
    try {
      final addresses = await _settingsUseCase.getAddresses();

      final AuthRepository authRepo = locator.get<AuthRepository>();
      final user = await authRepo.getUser();

      selectedAddress = addresses
          .where((element) => element.id == user.stopAddressId)
          .firstOrNull;

      emit(AddressesLoaded(
        addresses: addresses,
        selectedAddress: selectedAddress,
      ));
    } catch (e) {
      emit(AddressesFailure(message: e.toString()));
    }
  }

  Future<void> select(AddressEntity address, DateTime? dateTime) async {
    if (dateTime == null) {
      return;
    }
    try {
      if (state is AddressesLoaded) {
        final addressLoadedState = state as AddressesLoaded;
        await _settingsUseCase.updateStopAddress(
          address.id,
          dateTime,
        );
        emit(
          AddressesLoaded(
            addresses: addressLoadedState.addresses,
            selectedAddress: address,
          ),
        );
      }
    } catch (e) {
      emit(AddressesFailure(message: e.toString()));
    }
  }
}
