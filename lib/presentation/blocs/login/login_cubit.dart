import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/usecases/auth_usecase.dart';
import '../auth/auth_bloc.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final AuthUseCase _authUseCase;
  final AuthBloc _authBloc;

  LoginCubit(this._authUseCase, this._authBloc) : super(LoginInitial());

  Future<void> login({
    required String login,
    required String password,
  }) async {
    emit(LoginLoading());

    try {
      await _authUseCase.logIn(
        login: login,
        password: password,
      );
      emit(LoginSuccess());
      _authBloc.add(AuthCheckAuthenticationEvent());
    } catch (e) {
      debugPrint(e.toString());
      emit(LoginFailure(e.toString()));
    }
  }
}
