part of 'order_status_cubit.dart';

@immutable
abstract class OrderStatusState {}

class OrderStatusInitial extends OrderStatusState {}

class OrderStatusChanging extends OrderStatusState {}

class OrderStatusChanged extends OrderStatusState {}

class OrderStatusFailure extends OrderStatusState {
  final String message;

  OrderStatusFailure(this.message);
}
