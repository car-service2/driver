import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../domain/usecases/order_usecase.dart';

part 'order_status_state.dart';

class OrderStatusCubit extends Cubit<OrderStatusState> {
  final OrderUseCase _orderUseCase;

  OrderStatusCubit(this._orderUseCase) : super(OrderStatusInitial());

  Future<void> accept({
    required int orderId,
  }) async {
    try {
      emit(OrderStatusChanging());
      await _orderUseCase.accept(orderId);
      emit(OrderStatusChanged());
    } catch (e) {
      emit(OrderStatusFailure(e.toString()));
    } finally {
      emit(OrderStatusInitial());
    }
  }

  Future<void> start({
    required int orderId,
  }) async {
    try {
      emit(OrderStatusChanging());
      await _orderUseCase.start(orderId);
      emit(OrderStatusChanged());
    } catch (e) {
      emit(OrderStatusFailure(e.toString()));
    } finally {
      emit(OrderStatusInitial());
    }
  }

  Future<void> decline({
    required int orderId,
  }) async {
    try {
      emit(OrderStatusChanging());
      await _orderUseCase.decline(orderId);
      emit(OrderStatusChanged());
    } catch (e) {
      emit(OrderStatusFailure(e.toString()));
    } finally {
      emit(OrderStatusInitial());
    }
  }

  Future<void> endTrip({
    required int orderId,
    required double? orderValue,
    required double? orderCreditValue,
    required double? orderTolValue,
    required String? description,
  }) async {
    try {
      emit(OrderStatusChanging());
      await _orderUseCase.endTrip(
        orderId: orderId,
        orderValue: orderValue,
        description: description,
        orderCreditValue: orderCreditValue,
        orderTolValue: orderTolValue,
      );
      emit(OrderStatusChanged());
    } catch (e) {
      emit(OrderStatusFailure(e.toString()));
    } finally {
      emit(OrderStatusInitial());
    }
  }
}
