part of 'menu_cubit.dart';

enum Menu {
  orders,
  statistics,
  settings;

  @override
  String toString() {
    return switch (this) {
      Menu.orders => "My Orders",
      Menu.statistics => "Statistic",
      Menu.settings => "Driver",
    };
  }

  String get appBarTitle => switch (this) {
        Menu.orders => "My Orders",
        Menu.statistics => "Statistic",
        Menu.settings => "Driver Settings",
      };
}
