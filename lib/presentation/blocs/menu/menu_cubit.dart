import 'package:bloc/bloc.dart';

part 'menu_state.dart';

class MenuCubit extends Cubit<Menu> {
  MenuCubit() : super(Menu.orders);

  void set(Menu menu) => emit(menu);
}
