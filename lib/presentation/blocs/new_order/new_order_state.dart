part of 'new_order_bloc.dart';

@immutable
abstract class NewOrderState extends Equatable {}

class NewOrderInitial extends NewOrderState {
  @override
  List<Object?> get props => [];
}

class NewOrderReceived extends NewOrderState {
  final OrderEntity order;

  NewOrderReceived(this.order);

  @override
  List<Object?> get props => [order];
}
