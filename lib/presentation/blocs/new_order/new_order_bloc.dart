import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:bloc/bloc.dart';
import 'package:driver/domain/entities/order.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/usecases/new_order_usecase.dart';
import '../inline/inline_cubit.dart';

part 'new_order_event.dart';
part 'new_order_state.dart';

class NewOrderBloc extends Bloc<NewOrderEvent, NewOrderState> {
  final NewOrderUseCase _newOrderUseCase;
  final InlineCubit _inlineCubit;
  final player = AudioPlayer();

  NewOrderBloc(
    this._newOrderUseCase,
    this._inlineCubit,
  ) : super(NewOrderInitial()) {
    on<NewOrderReceivedEvent>(_onNewOrderReceived);
    _inlineCubit.stream.listen((InlineState inlineState) {
      if (inlineState is InlineChanged) {
        if (inlineState.inline) {
          _newOrderUseCase.startListeningForNewOrder(
            onNewOrderReceived: (order) => add(NewOrderReceivedEvent(order)),
            onError: (dynamic, StackTrace e) {
              debugPrint("Error getting new order!");
              debugPrint(e.toString());
            },
          );
        } else {
          _newOrderUseCase.stopListeningForNewOrder();
        }
      }
    });
  }

  FutureOr<void> _onNewOrderReceived(
    NewOrderReceivedEvent event,
    Emitter<NewOrderState> emit,
  ) async {
    await player.play(AssetSource('sounds/notification.wav'));
    emit(NewOrderReceived(event.order));
  }

  @override
  Future<void> close() {
    _newOrderUseCase.stopListeningForNewOrder();
    return super.close();
  }
}
