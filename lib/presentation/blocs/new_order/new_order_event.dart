part of 'new_order_bloc.dart';

@immutable
abstract class NewOrderEvent extends Equatable {}

class NewOrderReceivedEvent extends NewOrderEvent {
  final OrderEntity order;

  NewOrderReceivedEvent(this.order);

  @override
  List<Object?> get props => [order];
}
