part of 'create_order_cubit.dart';

@immutable
abstract class CreateOrderState {}

class CreateOrderInitial extends CreateOrderState {}

class CreateOrderLoading extends CreateOrderState {}

class CreateOrderCreated extends CreateOrderState {
  CreateOrderCreated();
}

class CreateOrderFailure extends CreateOrderState {}
