import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../domain/entities/order.dart';
import '../../../domain/usecases/order_usecase.dart';

part 'create_order_state.dart';

class CreateOrderCubit extends Cubit<CreateOrderState> {
  final OrderUseCase _orderUseCase;

  CreateOrderCubit(this._orderUseCase) : super(CreateOrderInitial());

  Future<void> create(OrderEntity order) async {
    try {
      emit(CreateOrderLoading());

      await _orderUseCase.create(order);

      emit(CreateOrderCreated());
    } catch (e) {
      debugPrint(e.toString());
      emit(CreateOrderFailure());
    }
  }
}
