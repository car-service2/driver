part of 'permissions_cubit.dart';

@immutable
abstract class PermissionsState {}

class PermissionsInitial extends PermissionsState {}

class PermissionsLoaded extends PermissionsState {
  final bool isRequested;
  final bool allowedNotification;
  final bool allowedGeolocation;
  final bool locationAlwaysIsGranted;

  PermissionsLoaded({
    required this.isRequested,
    required this.allowedNotification,
    required this.allowedGeolocation,
    required this.locationAlwaysIsGranted,
  });

  bool get granted =>
      allowedGeolocation && allowedNotification && locationAlwaysIsGranted;
}
