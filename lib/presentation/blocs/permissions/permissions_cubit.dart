import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:driver/domain/usecases/settings_usecase.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../firebase_messaging_service.dart';

part 'permissions_state.dart';

class PermissionsCubit extends Cubit<PermissionsState> {
  final FirebaseMessagingService _firebaseMessagingService;
  final SettingsUseCase _settingsUseCase;
  final _key = 'isFirstStart';

  PermissionsCubit(this._firebaseMessagingService, this._settingsUseCase)
      : super(PermissionsInitial());

  Future<void> requestNotification() async {
    await _checkPushNotificationPermission();
  }

  Future<void> requestGeolocation() async {
    await _checkGeolocationPermission();
    await _requestLocationAlways();
  }

  Future<void> start() async {
    
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(_key)) {
      await checkPermissions();
    } else {
      emit(
        PermissionsLoaded(
          allowedNotification: false,
          allowedGeolocation: false,
          locationAlwaysIsGranted: false,
          isRequested: false,
        ),
      );
    }
  }

  Future<void> checkPermissions() async {
    bool pushAllowed = await _checkPushNotificationPermission();
    bool locationAllowed = await _checkGeolocationPermission();
    bool locationAlwaysIsGranted = await _requestLocationAlways();
    debugPrint(
        "push enabled: $pushAllowed | location enabled: $locationAllowed");

    var backgroundService = FlutterBackgroundService();
    bool isBackgroundServiceRunning = await backgroundService.isRunning();
    if (locationAlwaysIsGranted && !isBackgroundServiceRunning) {
      if (Platform.isAndroid) {
        backgroundService.startService();
      }
    } else {
      _requestLocationAlways();
    }

    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_key, true);
    emit(PermissionsLoaded(
      allowedNotification: pushAllowed,
      allowedGeolocation: locationAllowed,
      locationAlwaysIsGranted: locationAlwaysIsGranted,
      isRequested: true,
    ));
  }

  Future<bool> _checkGeolocationPermission() async {
    LocationPermission permission = await Geolocator.checkPermission();
    bool hasPermission = _checkLocationPermission(permission);

    if (!hasPermission) {
      final permission = await Geolocator.requestPermission();
      return _checkLocationPermission(permission);
    }

    return hasPermission;
  }

  bool _checkLocationPermission(LocationPermission permission) {
    return switch (permission) {
      LocationPermission.whileInUse => true,
      LocationPermission.always => true,
      LocationPermission.denied => false,
      LocationPermission.deniedForever => false,
      _ => false,
    };
  }

  Future<bool> _checkPushNotificationPermission() async {
    final isEnabled = await _firebaseMessagingService.requestPermission();
    if (isEnabled) {
      _settingsUseCase.setFSMToken();
    }
    return isEnabled;
  }

  Future<bool> _requestLocationAlways() async {
    bool always = await Permission.locationAlways.isGranted;

    if (always == false) {
      var status = await Permission.locationAlways.request();
      debugPrint("always is granted:${status.isGranted}");

      return status.isGranted;
    } else {
      return always;
    }
  }
}
