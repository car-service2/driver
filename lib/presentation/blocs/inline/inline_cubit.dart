import 'package:bloc/bloc.dart';
import 'package:driver/domain/entities/user.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../domain/usecases/settings_usecase.dart';

part 'inline_state.dart';

class InlineCubit extends Cubit<InlineState> {
  final SettingsUseCase _settingsUseCase;

  InlineCubit(this._settingsUseCase) : super(InlineInitial());

  Future<void> load(UserEntity authorizedUser) async {
    emit(InlineChanging());

    try {
      emit(InlineChanged(authorizedUser.inLine));
    } catch (e) {
      emit(InlineFailure(message: e.toString()));
    }
  }

  Future<void> change() async {
    emit(InlineChanging());
    try {
      final inline = await _settingsUseCase.updateInline();
      emit(InlineChanged(inline));
    } catch (e) {
      emit(InlineFailure(message: e.toString()));
    }
  }
}
