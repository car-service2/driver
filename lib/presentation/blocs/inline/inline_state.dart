part of 'inline_cubit.dart';

@immutable
abstract class InlineState extends Equatable {}

class InlineInitial extends InlineState {
  @override
  List<Object?> get props => [];
}

class InlineChanging extends InlineState {
  @override
  List<Object?> get props => [];
}

class InlineChanged extends InlineState {
  final bool inline;

  InlineChanged(this.inline);

  @override
  List<Object?> get props => [inline];
}

class InlineFailure extends InlineState {
  final String message;

  InlineFailure({required this.message});

  @override
  List<Object?> get props => [message];
}
