import 'package:driver/presentation/blocs/auth/auth_bloc.dart';
import 'package:driver/presentation/pages/main_page.dart';
import 'package:driver/presentation/pages/permission_wrapper.dart';
import 'package:driver/presentation/pages/sign_in_page.dart';
import 'package:driver/presentation/theme/app_theme.dart';
import 'package:driver/presentation/widgets/failure_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Application extends StatelessWidget {
  const Application({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Driver Application',
      theme: AppTheme.themeData,
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return MediaQuery(
          data:
          MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
          child: child!,
        );
      },
      home: BlocBuilder<AuthBloc, AuthState>(
        bloc: BlocProvider.of<AuthBloc>(context),
        builder: (_, state) {
          if (state is AuthUnauthenticated) {
            return const SignInPage();
          }

          if (state is AuthError) {
            return Scaffold(
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FailureWidget(
                      message: state.message,
                      onPressed: () {
                        BlocProvider.of<AuthBloc>(context).add(
                          AuthCheckAuthenticationEvent(),
                        );
                      },
                    ),
                  ],
                ),
              ),
            );
          }

          if (state is AuthAuthenticated) {
            return const PermissionsWrapper(
              child: MainPage(),
            );
          }

          if (state is AuthInitial || state is AuthLoading) {
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(
                  color: Colors.white,
                  strokeWidth: 1.5,
                ),
              ),
            );
          }

          return const Scaffold(
            body: Center(
              child: Text("Unhandled Auth state!"),
            ),
          );
        },
      ),
    );
  }
}
