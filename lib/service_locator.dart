import 'package:dio/dio.dart';
import 'package:driver/firebase_messaging_service.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'data/datasources/interceptors/token_interceptor.dart';
import 'data/datasources/interfaces/auth_datasource.dart';
import 'data/datasources/interfaces/orders_datasource.dart';
import 'data/datasources/interfaces/settings_datasourse.dart';
import 'data/datasources/interfaces/statistics_datasource.dart';
import 'data/datasources/local/fcm_token_storage.dart';
import 'data/datasources/local/jwt_token_storage.dart';
import 'data/datasources/remote/api/auth_api_provider.dart';
import 'data/datasources/remote/api/orders_api_provider.dart';
import 'data/datasources/remote/api/settings_api_provider.dart';
import 'data/datasources/remote/api/statistics_api_provider.dart';
import 'data/datasources/remote/streams/order_stream.dart';
import 'data/mappers/mapper.dart';
import 'data/repositories/auth_repository.dart';
import 'data/repositories/order_repository.dart';
import 'data/repositories/order_stream_repository.dart';
import 'data/repositories/settings_repository.dart';
import 'data/repositories/statistics_repository.dart';
import 'domain/repositories/auth_repository.dart';
import 'domain/repositories/order_repository.dart';
import 'domain/repositories/order_stream_repository.dart';
import 'domain/repositories/settings_repository.dart';
import 'domain/repositories/statistics_repository.dart';
import 'domain/usecases/auth_usecase.dart';
import 'domain/usecases/new_order_usecase.dart';
import 'domain/usecases/order_usecase.dart';
import 'domain/usecases/settings_usecase.dart';
import 'domain/usecases/statistics_usecase.dart';
import 'presentation/blocs/addresses/addresses_cubit.dart';
import 'presentation/blocs/auth/auth_bloc.dart';
import 'presentation/blocs/create_order/create_order_cubit.dart';
import 'presentation/blocs/inline/inline_cubit.dart';
import 'presentation/blocs/login/login_cubit.dart';
import 'presentation/blocs/logout/logout_cubit.dart';
import 'presentation/blocs/menu/menu_cubit.dart';
import 'presentation/blocs/new_order/new_order_bloc.dart';
import 'presentation/blocs/order_status/order_status_cubit.dart';
import 'presentation/blocs/orders/orders_cubit.dart';
import 'presentation/blocs/permissions/permissions_cubit.dart';
import 'presentation/blocs/statistics/statistics_cubit.dart';

final locator = GetIt.instance;
const apiBaseUrl = 'http://arideinc.com';

Future<void> setupLocator() async {
  // Регистрируем зависимости
  final sharedPreferences = await SharedPreferences.getInstance();
  await sharedPreferences.reload();

  locator.registerFactory<SharedPreferences>(() => sharedPreferences);
  locator.registerLazySingleton<Dio>(() => Dio()
    ..interceptors.add(TokenInterceptor(JwtTokenStorage(sharedPreferences))));

  if (kDebugMode) {
    locator.get<Dio>().interceptors.add(PrettyDioLogger(requestBody: true));
  }

  /// data sources
  locator.registerLazySingleton<AuthDataSource>(
      () => AuthApiProvider(locator<Dio>(), apiBaseUrl));
  locator.registerLazySingleton<SettingsDataSource>(
      () => SettingsApiProvider(locator<Dio>(), apiBaseUrl));
  locator.registerLazySingleton<OrderDataSource>(
      () => OrderApiProvider(locator<Dio>(), apiBaseUrl));
  locator.registerLazySingleton<StatisticsDataSource>(
      () => StatisticsApiProvider(locator<Dio>(), apiBaseUrl));
  locator.registerLazySingleton<OrderStream>(
      () => OrderStream()..startListening());

  /// mappers
  locator.registerLazySingleton<UserMapper>(() => UserMapper());
  locator.registerLazySingleton<TokenMapper>(() => TokenMapper());
  locator.registerLazySingleton<AddressMapper>(() => AddressMapper());
  locator.registerLazySingleton<OrderMapper>(() => OrderMapper());
  locator.registerLazySingleton<StatisticMapper>(() => StatisticMapper());

  /// repositories
  locator.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImpl(
      locator<AuthDataSource>(),
      locator<UserMapper>(),
      locator<TokenMapper>(),
    ),
  );
  locator.registerLazySingleton<SettingsRepository>(
    () => SettingsRepositoryImpl(locator<SettingsDataSource>(),
        locator<AddressMapper>(), locator<UserMapper>()),
  );
  locator.registerLazySingleton<OrderRepository>(() =>
      OrderRepositoryImpl(locator<OrderDataSource>(), locator<OrderMapper>()));
  locator.registerLazySingleton<StatisticsRepository>(
    () => StatisticsRepositoryImpl(
      locator<StatisticsDataSource>(),
      locator<StatisticMapper>(),
    ),
  );
  locator.registerLazySingleton<OrderStreamRepository>(
    () => OrderStreamRepositoryImpl(
      locator<OrderStream>(),
      locator<OrderMapper>(),
    ),
  );
  locator.registerLazySingleton(
    () => FirebaseMessagingService(),
  );

  /// use cases
  locator.registerLazySingleton<AuthUseCase>(() => AuthUseCase(
      locator<AuthRepository>(),
      JwtTokenStorage(locator<SharedPreferences>())));
  locator.registerLazySingleton<SettingsUseCase>(
    () => SettingsUseCase(
      locator<SettingsRepository>(),
      locator<FirebaseMessagingService>(),
      FCMTokenStorage(sharedPreferences),
    ),
  );
  locator.registerLazySingleton<OrderUseCase>(
      () => OrderUseCase(locator<OrderRepository>()));
  locator.registerLazySingleton<StatisticsUseCase>(
      () => StatisticsUseCase(locator<StatisticsRepository>()));
  locator.registerLazySingleton<NewOrderUseCase>(
      () => NewOrderUseCase(locator<OrderStreamRepository>()));

  /// state manages blocs, cubits
  locator.registerLazySingleton<AddressesCubit>(
    () => AddressesCubit(locator<SettingsUseCase>()),
  );
  locator.registerLazySingleton<InlineCubit>(
    () => InlineCubit(locator<SettingsUseCase>()),
  );
  locator.registerLazySingleton<NewOrderBloc>(
    () => NewOrderBloc(
      locator<NewOrderUseCase>(),
      locator<InlineCubit>(),
    ),
  );
  locator.registerLazySingleton<PermissionsCubit>(
    () => PermissionsCubit(
      locator<FirebaseMessagingService>(),
      locator<SettingsUseCase>(),
    ),
  );
  locator.registerLazySingleton<AuthBloc>(
    () => AuthBloc(locator<AuthUseCase>()),
  );
  locator.registerLazySingleton<LoginCubit>(
    () => LoginCubit(
      locator<AuthUseCase>(),
      locator<AuthBloc>(),
    ),
  );
  locator.registerLazySingleton<LogoutCubit>(
    () => LogoutCubit(
      locator<AuthUseCase>(),
      locator<AuthBloc>(),
    ),
  );
  locator.registerLazySingleton<MenuCubit>(() => MenuCubit());
  locator.registerLazySingleton<OrdersCubit>(
    () => OrdersCubit(locator<OrderUseCase>()),
  );
  locator.registerLazySingleton<OrderStatusCubit>(
    () => OrderStatusCubit(locator<OrderUseCase>()),
  );
  locator.registerLazySingleton<StatisticsDailyCubit>(
    () => StatisticsDailyCubit(locator<StatisticsUseCase>()),
  );
  locator.registerLazySingleton<StatisticsCustomCubit>(
    () => StatisticsCustomCubit(locator<StatisticsUseCase>()),
  );
  locator.registerFactory<CreateOrderCubit>(
    () => CreateOrderCubit(locator.get<OrderUseCase>()),
  );
}
