import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:driver/domain/entities/user.dart';
import 'package:driver/domain/repositories/auth_repository.dart';
import 'package:driver/service_locator.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'domain/repositories/settings_repository.dart';

Future<void> initializeService() async {
  if (Platform.isIOS) {
    const locationEventChannel = EventChannel('location_event_channel');
    var settingsRepo = locator.get<SettingsRepository>();

    locationEventChannel.receiveBroadcastStream().listen((location) {
      settingsRepo
          .setLocation(
            longitude: double.parse(location['longitude'].toString()),
            latitude: double.parse(location['latitude'].toString()),
          )
          .then((value) => debugPrint(location.toString()))
          .catchError((error) => debugPrint(error.toString()));
    });

    return;
  }

  final service = FlutterBackgroundService();

  await service.configure(
    androidConfiguration: AndroidConfiguration(
      // this will be executed when app is in foreground or background in separated isolate
      onStart: onForeground,
      // auto start service
      autoStart: false,
      isForegroundMode: true,
    ),
    iosConfiguration: IosConfiguration(
      // auto start service
      autoStart: false,

      // this will be executed when app is in foreground in separated isolate
      onForeground: onForeground,

      // you have to enable background fetch capability on xcode project
      onBackground: onBackground,
    ),
  );
}

// to ensure this is executed
// run app from xcode, then from xcode menu, select Simulate Background Fetch

@pragma('vm:entry-point')
Future<bool> onBackground(ServiceInstance service) async {
  WidgetsFlutterBinding.ensureInitialized();
  DartPluginRegistrant.ensureInitialized();

  await setupLocator();

  await updateGeolocation();

  return true;
}

@pragma('vm:entry-point')
void onForeground(ServiceInstance service) async {
  // Only available for flutter 3.0.0 and later
  DartPluginRegistrant.ensureInitialized();

  await setupLocator();

  // For flutter prior to version 3.0.0
  // We have to register the plugin manually

  if (service is AndroidServiceInstance) {
    service.on('setAsForeground').listen((event) {
      service.setAsForegroundService();
    });

    service.on('setAsBackground').listen((event) {
      service.setAsBackgroundService();
    });
  }

  service.on('stopService').listen((event) {
    service.stopSelf();
  });

  debugPrint("onForeground");
  await updateGeolocation();
}

Future<void> updateGeolocation() async {
  try {
    var sharedPrefs = await SharedPreferences.getInstance();
    var permission = await Geolocator.checkPermission();

    bool hasLocationPermission = permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse;
    bool isEnabled = await Geolocator.isLocationServiceEnabled();

    debugPrint("get user");
    UserEntity? user;

    try {
      user = await locator.get<AuthRepository>().getUser();
    } catch (e) {
      debugPrint(e.toString());
    }

    bool isUserAuthenticated = user != null;

    debugPrint(
      "isEnabled: $isEnabled | hasLocationPermission:$hasLocationPermission | isUserAuthenticated: $isUserAuthenticated",
    );

    if (isEnabled && hasLocationPermission) {
      Timer.periodic(const Duration(seconds: 5), (timer) async {
        var position = await Geolocator.getCurrentPosition();

        await sharedPrefs.reload();

        debugPrint(
            "${DateTime.now()}| ${position.latitude}|${position.longitude}");

        var settingsRepo = locator.get<SettingsRepository>();

        await settingsRepo.setLocation(
          longitude: position.longitude,
          latitude: position.latitude,
        );

        if (kDebugMode) {
          sharedPrefs.setString(
            'locationChanged',
            json.encode(position.toJson()),
          );
        }
      });
    } else {
      debugPrint("LocationService: Disabled");
    }
  } catch (e) {
    debugPrint("Errrorrrr");
  }
}