import 'package:driver/data/datasources/interfaces/token_datasource.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FCMTokenStorage implements TokenDataSource {
  static const String _tokenKey = 'fcm-token';
  final SharedPreferences _sharedPreferences;

  FCMTokenStorage(this._sharedPreferences);

  @override
  String? getToken() {
    return _sharedPreferences.getString(_tokenKey);
  }

  @override
  Future<bool> setToken(String token) async {
    return await _sharedPreferences.setString(_tokenKey, token);
  }

  @override
  Future<bool> deleteToken() async {
    return await _sharedPreferences.remove(_tokenKey);
  }
}
