import 'package:driver/data/datasources/interfaces/token_datasource.dart';
import 'package:shared_preferences/shared_preferences.dart';

class JwtTokenStorage implements TokenDataSource {
  static const String _tokenKey = 'jwt-token';
  final SharedPreferences _sharedPreferences;

  JwtTokenStorage(this._sharedPreferences);

  @override
  String? getToken() {
    return _sharedPreferences.getString(_tokenKey);
  }

  @override
  Future<bool> setToken(String token) async {
    return await _sharedPreferences.setString(_tokenKey, token);
  }

  @override
  Future<bool> deleteToken() async {
    return await _sharedPreferences.remove(_tokenKey);
  }
}
