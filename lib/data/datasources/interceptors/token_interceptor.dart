import 'package:dio/dio.dart';
import 'package:driver/data/datasources/interfaces/token_datasource.dart';

class TokenInterceptor extends Interceptor {
  final TokenDataSource tokenStorage;

  TokenInterceptor(this.tokenStorage);

  @override
  void onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    final String? accessToken = tokenStorage.getToken();
    options.headers['Content-Type'] = 'Application/json';
    options.headers['Accept'] = 'Application/json';

    if (accessToken != null) {
      options.headers['Authorization'] = 'Bearer $accessToken';
    }

    handler.next(options);
  }
}
