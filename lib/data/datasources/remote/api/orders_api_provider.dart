import 'package:driver/data/datasources/remote/api/api_provider.dart';
import 'package:driver/data/models/order.dart';

import '../../interfaces/orders_datasource.dart';

class OrderApiProvider extends ApiProvider implements OrderDataSource {
  OrderApiProvider(super.dio, super.baseUrl);

  @override
  Future<void> acceptOrder(int orderId) async {
    await put(
      "$baseUrl/api/orders/accept",
      queryParams: {"order_id": orderId},
    );
  }

  @override
  Future<void> declineOrder(int orderId) async {
    await put(
      "$baseUrl/api/orders/decline",
      queryParams: {"order_id": orderId},
    );
  }

  @override
  Future<void> endTrip({
    required int orderId,
    required double? orderValue,
    required double? orderCreditValue,
    required double? orderTolValue,
    required String? description,
  }) async {
    await put("$baseUrl/api/orders/endtrip", data: {
      "order_id": orderId,
      "order_value": orderValue,
      "description": description,
      "order_credit_value": orderCreditValue,
      "order_toll_value": orderTolValue
    });
  }

  @override
  Future<List<OrderModel>> getOrders({
    required String status,
    required int page,
    required int perPage,
  }) async {
    final json = await get<Map<String, dynamic>>(
      "$baseUrl/api/orders",
      queryParams: {
        "status": status,
        "page": page,
        "perPage": perPage,
      },
    );
    final List list = List.from(json['items']);
    return list.map((json) => OrderModel.fromJson(json)).toList();
  }

  @override
  Future<void> start(int orderId) async {
    await put(
      "$baseUrl/api/orders/start",
      queryParams: {"order_id": orderId},
    );
  }

  @override
  Future<void> create(OrderModel orderModel) async {
    await post("$baseUrl/api/orders/create", data: orderModel.toJson());
  }

  @override
  Future<OrderModel> getById(String id) async {
    final json = await get<Map<String, dynamic>>("$baseUrl/api/orders/$id");
    return OrderModel.fromJson(json);
  }
}
