import 'package:dio/dio.dart';
import 'package:driver/data/datasources/interfaces/settings_datasourse.dart';
import 'package:driver/data/datasources/remote/api/api_provider.dart';
import 'package:driver/data/models/address_model.dart';
import 'package:driver/data/models/user_model.dart';

class SettingsApiProvider extends ApiProvider implements SettingsDataSource {
  SettingsApiProvider(Dio dio, String baseUrl) : super(dio, baseUrl);

  @override
  Future<List<AddressModel>> getAddresses() async {
    final json = await get<List>("${super.baseUrl}/api/get/address");
    return json.map((e) => AddressModel.fromJson(e)).toList();
  }

  @override
  Future<bool> updateInlineStatus() async {
    final json = await get<Json>("${super.baseUrl}/api/inline/");
    return json['in_line'] as bool;
  }

  @override
  Future<void> updateLocation({
    required double latitude,
    required double longitude,
  }) async {
    await put(
      "${super.baseUrl}/api/location",
      data: {
        "long": "$longitude",
        "lat": "$latitude",
      },
    );
  }

  @override
  Future<UserModel> updateStopAddress(int addressId, DateTime time) async {
    var json = await put(
      "${super.baseUrl}/api/stop/address",
      queryParams: {"stop_address_id": addressId},
      data: {"time": time.toUtc().millisecondsSinceEpoch ~/ 1000},
    );
    return UserModel.fromJson(json);
  }

  @override
  Future<void> saveFCMToken(String token) async {
    await put(
      "${super.baseUrl}/api/fcm_token",
      data: {"fcm_token": token},
    );
  }
}
