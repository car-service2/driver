import 'package:dio/dio.dart';
import 'package:driver/data/datasources/interfaces/statistics_datasource.dart';
import 'package:driver/data/datasources/remote/api/api_provider.dart';
import 'package:driver/data/models/statistic_model.dart';

class StatisticsApiProvider extends ApiProvider
    implements StatisticsDataSource {
  StatisticsApiProvider(Dio dio, String baseUrl) : super(dio, baseUrl);

  @override
  Future<StatisticModel> custom({
    required String startDate,
    required String endData,
  }) async {
    final json = await get(
      "$baseUrl/api/statistic/custom",
      queryParams: {
        "start_date": startDate,
        "end_date": endData,
      },
    );

    return StatisticModel(
      count: json['count'] as num,
      credit: json['credit'] as num,
      cash: json['cash'] as num,
      toll: json['toll_value'] as num,
      total: json['total'] as num,
      percentage: json['percentage'] as num,
    );
  }

  @override
  Future<StatisticModel> daily() async {
    final json = await get("$baseUrl/api/statistic/daily");

    return StatisticModel(
      count: json['count'] as num,
      credit: json['credit'] as num,
      cash: json['cash'] as num,
      total: json['total'] as num,
      toll: json['toll_value'] as num,
      percentage: json['percentage'] as num,
    );
  }
}
