import 'dart:io';

import 'package:dio/dio.dart';

typedef Json = Map<String, dynamic>;

abstract class ApiProvider {
  final String baseUrl;
  final Dio _dio;

  ApiProvider(this._dio, this.baseUrl);

  Future<T> get<T>(String url, {Json? queryParams, Json? data}) async {
    try {
      final response = await _dio.get(
        url,
        data: data,
        queryParameters: queryParams,
      );

      return response.data as T;
    } on DioException catch (e) {
      if (e.error is SocketException) {
        return throw "No internet connection";
      }
      rethrow;
    } catch (e) {
      throw Exception('Failed to get data: $e');
    }
  }

  Future<T> post<T>(String url, {Json? data, Json? queryParams}) async {
    try {
      final response = await _dio.post(
        url,
        data: data,
        queryParameters: queryParams,
      );

      return response.data as T;
    } on DioException catch (e) {
      if (e.error is SocketException) {
        return throw "No internet connection";
      }
      rethrow;
    } catch (e) {
      throw Exception('Failed to post data: $e');
    }
  }

  Future<T> put<T>(String url, {Json? data, Json? queryParams}) async {
    try {
      final response = await _dio.put(
        url,
        data: data,
        queryParameters: queryParams,
      );

      return response.data as T;
    } on DioException catch (e) {
      if (e.error is SocketException) {
        return throw "No internet connection";
      }
      return throw e.message ?? e.error.toString();
    } catch (e) {
      throw Exception('Failed to put data: $e');
    }
  }
}
