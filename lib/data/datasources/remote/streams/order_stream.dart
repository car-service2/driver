import 'dart:async';

import 'package:dio/dio.dart';
import 'package:driver/data/datasources/interfaces/stream_datasource.dart';
import 'package:driver/data/datasources/remote/api/orders_api_provider.dart';
import 'package:driver/data/models/order.dart';
import 'package:driver/service_locator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';

class OrderStream implements StreamDataSource<OrderModel> {
  late OrderApiProvider orderApi;
  late StreamController<OrderModel> _orderStreamController;

  OrderStream() {
    _orderStreamController = StreamController<OrderModel>.broadcast();
    orderApi = OrderApiProvider(locator<Dio>(), apiBaseUrl);
  }

  @override
  void startListening() {
    // on application open from terminated
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        onListenPushNotification(message.data);
      }
    }).catchError((error) {
      debugPrint(error.toString());
    });

    // on application open from foreground
    FirebaseMessaging.onMessageOpenedApp
        .map((event) => event.data)
        .listen(onListenPushNotification);

    // on message coming
    FirebaseMessaging.onMessage
        .map((event) => event.data)
        .listen(onListenPushNotification);
  }

  void onListenPushNotification(Map<String, dynamic> data) {
    debugPrint("onListenMessage");
    debugPrint(data.toString());
    if (data['type'] == 'order') {
      orderApi.getById(data['id'].toString()).then((OrderModel order) {
        _orderStreamController.add(order);
      }).catchError((e) {
        debugPrint(e.toString());
      });
    }
  }

  @override
  void dispose() {
    _orderStreamController.close();
  }

  @override
  Stream<OrderModel> get stream => _orderStreamController.stream;
}
