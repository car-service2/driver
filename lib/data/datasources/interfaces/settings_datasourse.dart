import '../../models/address_model.dart';
import '../../models/user_model.dart';

abstract class SettingsDataSource {
  Future<List<AddressModel>> getAddresses();

  Future<UserModel> updateStopAddress(int addressId, DateTime time);

  Future<bool> updateInlineStatus();

  Future<void> updateLocation({
    required double latitude,
    required double longitude,
  });

  Future<void> saveFCMToken(String token);
}
