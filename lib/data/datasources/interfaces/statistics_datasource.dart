import '../../models/statistic_model.dart';

abstract class StatisticsDataSource {
  Future<StatisticModel> daily();

  Future<StatisticModel> custom({
    required String startDate,
    required String endData,
  });
}
