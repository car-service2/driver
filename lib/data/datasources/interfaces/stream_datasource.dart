import 'dart:async';

abstract class StreamDataSource<T> {
  Stream<T> get stream;

  void startListening();

  void dispose();
}
