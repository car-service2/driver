import '../../models/order.dart';

abstract class OrderDataSource {
  Future<List<OrderModel>> getOrders({
    required String status,
    required int page,
    required int perPage,
  });

  Future<void> acceptOrder(int orderId);

  Future<void> declineOrder(int orderId);

  Future<void> endTrip({
    required int orderId,
    required double? orderValue,
    required double? orderCreditValue,
    required double? orderTolValue,
    required String? description,
  });

  Future<void> start(int orderId);

  Future<void> create(OrderModel orderModel);

  Future<OrderModel> getById(String id);
}
