import 'package:driver/data/datasources/interfaces/stream_datasource.dart';
import 'package:driver/domain/entities/order.dart';
import 'package:flutter/foundation.dart';

import '../../domain/repositories/order_stream_repository.dart';
import '../mappers/mapper.dart';
import '../models/order.dart';

class OrderStreamRepositoryImpl implements OrderStreamRepository {
  final StreamDataSource<OrderModel> _orderModelStreamDataSource;
  final Mapper<OrderEntity, OrderModel> _orderMapper;

  OrderStreamRepositoryImpl(
    this._orderModelStreamDataSource,
    this._orderMapper,
  );

  @override
  Stream<OrderEntity> getOrderStream() {
    return _orderModelStreamDataSource.stream.map((model) {
      debugPrint("model: $model");
      final entity = _orderMapper.fromModel(model);
      debugPrint("entity: $entity");
      return entity;
    });
  }
}
