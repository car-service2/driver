import '../../domain/entities/token.dart';
import '../../domain/entities/user.dart';
import '../../domain/repositories/auth_repository.dart';
import '../datasources/interfaces/auth_datasource.dart';
import '../mappers/mapper.dart';
import '../models/token_model.dart';
import '../models/user_model.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthDataSource authApiProvider;
  final Mapper<UserEntity, UserModel> userMapper;
  final Mapper<TokenEntity, TokenModel> tokenMapper;

  AuthRepositoryImpl(
    this.authApiProvider,
    this.userMapper,
    this.tokenMapper,
  );

  @override
  Future<TokenEntity> logIn({
    required String login,
    required String password,
  }) async {
    final token = await authApiProvider.login(
      login: login,
      password: password,
    );

    return tokenMapper.fromModel(token);
  }

  @override
  Future<void> logOut() async {
    await authApiProvider.logout();
  }

  @override
  Future<UserEntity> getUser() async {
    final UserModel user = await authApiProvider.me();
    return userMapper.fromModel(user);
  }
}
