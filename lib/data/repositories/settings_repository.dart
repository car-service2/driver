import 'package:driver/data/datasources/interfaces/settings_datasourse.dart';
import 'package:driver/data/mappers/mapper.dart';
import 'package:driver/data/models/address_model.dart';
import 'package:driver/data/models/user_model.dart';
import 'package:driver/domain/entities/address.dart';
import 'package:driver/domain/entities/user.dart';
import 'package:driver/domain/repositories/settings_repository.dart';

class SettingsRepositoryImpl implements SettingsRepository {
  final SettingsDataSource settingsDataSource;
  final Mapper<AddressEntity, AddressModel> addressMapper;
  final Mapper<UserEntity, UserModel> userMapper;

  SettingsRepositoryImpl(
    this.settingsDataSource,
    this.addressMapper,
    this.userMapper,
  );

  @override
  Future<bool> inLine() async {
    return await settingsDataSource.updateInlineStatus();
  }

  @override
  Future<void> setFCMToken(String token) async {
    await settingsDataSource.saveFCMToken(token);
  }

  @override
  Future<void> setLocation({
    required double longitude,
    required double latitude,
  }) async {
    await settingsDataSource.updateLocation(
      latitude: latitude,
      longitude: longitude,
    );
  }

  @override
  Future<UserEntity> updateStopAddress(int addressId, DateTime dateTime) async {
    final userModel = await settingsDataSource.updateStopAddress(addressId, dateTime);
    return userMapper.fromModel(userModel);
  }

  @override
  Future<List<AddressEntity>> getAddresses() async {
    final addressesModelList = await settingsDataSource.getAddresses();
    final List<AddressEntity> addressesEntityList = addressesModelList
        .map((addressModel) => addressMapper.fromModel(addressModel))
        .toList();
    return addressesEntityList;
  }
}
