import 'package:driver/data/datasources/interfaces/statistics_datasource.dart';
import 'package:driver/data/models/statistic_model.dart';
import 'package:driver/domain/entities/statistic.dart';

import '../../domain/repositories/statistics_repository.dart';
import '../mappers/mapper.dart';

class StatisticsRepositoryImpl implements StatisticsRepository {
  final StatisticsDataSource _statisticsDataSource;
  final Mapper<StatisticEntity, StatisticModel> _statisticMapper;

  StatisticsRepositoryImpl(
    this._statisticsDataSource,
    this._statisticMapper,
  );

  @override
  Future<StatisticEntity> custom({
    required String startDate,
    required String endDate,
  }) async {
    final model = await _statisticsDataSource.custom(
      startDate: startDate,
      endData: endDate,
    );

    return _statisticMapper.fromModel(model);
  }

  @override
  Future<StatisticEntity> daily() async {
    final model = await _statisticsDataSource.daily();
    return _statisticMapper.fromModel(model);
  }
}
