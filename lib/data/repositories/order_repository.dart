import 'package:driver/data/datasources/interfaces/orders_datasource.dart';
import 'package:driver/data/mappers/mapper.dart';
import 'package:driver/domain/entities/order.dart';
import 'package:driver/domain/repositories/order_repository.dart';

import '../models/order.dart';

class OrderRepositoryImpl implements OrderRepository {
  final OrderDataSource orderDataSource;
  final Mapper<OrderEntity, OrderModel> orderMapper;

  OrderRepositoryImpl(this.orderDataSource, this.orderMapper);

  @override
  Future<List<OrderEntity>> getOrders({
    required String status,
    required int page,
    required int perPage,
  }) async {
    final List<OrderModel> orders = await orderDataSource.getOrders(
      status: status,
      page: page,
      perPage: perPage,
    );

    return orders.map((model) => orderMapper.fromModel(model)).toList();
  }

  @override
  Future<void> accept({required int orderId}) async {
    await orderDataSource.acceptOrder(orderId);
  }

  @override
  Future<void> decline({required int orderId}) async {
    await orderDataSource.declineOrder(orderId);
  }

  @override
  Future<void> endTrip({
    required int orderId,
    required double? orderValue,
    required double? orderCreditValue,
    required double? orderTolValue,
    String? description,
  }) async {
    await orderDataSource.endTrip(
      orderId: orderId,
      orderValue: orderValue,
      orderCreditValue: orderCreditValue,
      orderTolValue: orderTolValue,
      description: description,
    );
  }

  @override
  Future<void> start({required int orderId}) async {
    await orderDataSource.start(orderId);
  }

  @override
  Future<void> create(OrderEntity order) async {
    return await orderDataSource.create(orderMapper.toModel(order));
  }
}
