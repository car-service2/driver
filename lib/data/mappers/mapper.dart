import 'package:driver/data/models/address_model.dart';
import 'package:driver/data/models/statistic_model.dart';
import 'package:driver/data/models/token_model.dart';
import 'package:driver/data/models/user_model.dart';
import 'package:driver/domain/entities/address.dart';
import 'package:driver/domain/entities/statistic.dart';
import 'package:driver/domain/entities/token.dart';
import 'package:driver/domain/entities/user.dart';

import '../../domain/entities/order.dart';
import '../models/order.dart';

abstract class Mapper<E, M> {
  E fromModel(M model);

  M toModel(E entity);
}

class UserMapper implements Mapper<UserEntity, UserModel> {
  @override
  UserEntity fromModel(UserModel model) {
    return UserEntity(
      id: model.id,
      name: model.name,
      login: model.login,
      avatar: model.image,
      inLine: model.inLine,
      stopAddressId: model.stopAddressId,
    );
  }

  @override
  UserModel toModel(UserEntity entity) => throw UnimplementedError();
}

class TokenMapper implements Mapper<TokenEntity, TokenModel> {
  @override
  TokenEntity fromModel(TokenModel model) {
    return TokenEntity(
      accessToken: model.accessToken,
      tokenType: model.tokenType,
      expiresIn: model.expiresIn,
    );
  }

  @override
  TokenModel toModel(TokenEntity entity) => throw UnimplementedError();
}

class AddressMapper implements Mapper<AddressEntity, AddressModel> {
  @override
  AddressEntity fromModel(AddressModel model) {
    return AddressEntity(id: model.id, name: model.name);
  }

  @override
  AddressModel toModel(AddressEntity entity) {
    return AddressModel(id: entity.id, name: entity.name);
  }
}

class OrderMapper implements Mapper<OrderEntity, OrderModel> {
  @override
  OrderEntity fromModel(OrderModel model) {
    return OrderEntity(
      id: model.id,
      customer: model.customerName ?? "",
      customerPhone: model.customerPhone.toString(),
      date: model.date,
      status: OrderStatusEntity.fromString(model.status),
      paymentType: model.paymentType,
      toll: model.toll,
      waypoints: model.waypoints,
      important: model.important,
      desciption: model.description ?? '',
      orderValue: model.orderValue ?? 0,
      orderCreditValue: model.orderCreditValue ?? 0,
      orderTollValue: model.orderTollValue ?? 0,
    );
  }

  @override
  OrderModel toModel(OrderEntity entity) {
    return OrderModel(
      id: 0,
      date: entity.date.toString(),
      waypoints: entity.waypoints,
      status: '',
      customerPhone: entity.customerPhone,
      orderValue: double.tryParse(entity.orderValue.toString()) ?? 0,
      paymentType: '',
      important: false,
      toll: false,
      description: entity.desciption,
      orderCreditValue: entity.orderCreditValue,
      orderTollValue: entity.orderTollValue,
    );
  }
}

class StatisticMapper implements Mapper<StatisticEntity, StatisticModel> {
  @override
  StatisticEntity fromModel(StatisticModel model) {
    return StatisticEntity(
      count: model.count,
      credit: model.credit,
      toll: model.toll,
      cash: model.cash,
      total: model.total,
      percentage: model.percentage,
    );
  }

  @override
  StatisticModel toModel(StatisticEntity entity) => throw UnimplementedError();
}
