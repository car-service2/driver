class Waypoint {
  int rt;
  int passengers;
  String address;
  double latitude;
  double longitude;

  Waypoint({
    required this.rt,
    required this.passengers,
    required this.address,
    required this.latitude,
    required this.longitude,
  });

  Map<String, Object> toJson() {
    return {
      "rt": rt,
      "passengers": passengers,
      "address": address,
      "latitude": latitude,
      "longitude": longitude,
    };
  }
}
