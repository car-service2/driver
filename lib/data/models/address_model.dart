class AddressModel {
  final int id;
  final String name;

  AddressModel({
    required this.id,
    required this.name,
  });

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
      id: json['id'],
      name: json['name'],
    );
  }
}
