import 'package:driver/data/models/waypoint.dart';

class OrderModel {
  final int id;
  final List<Waypoint> waypoints;
  final String date;
  final String status;
  final String? customerName;
  final String customerPhone;
  final num? orderValue;
  final String paymentType;
  final bool important;
  final String? description;
  final bool toll;

  OrderModel({
    required this.id,
    required this.date,
    required this.waypoints,
    required this.status,
    this.customerName,
    required this.customerPhone,
    required this.orderValue,
    required this.paymentType,
    required this.important,
    this.description,
    required this.toll,
  });

  factory OrderModel.fromJson(Map<String, dynamic> json) {
    final bool isCash = json['payment_type'] == 'cash';
    final num? orderValue = isCash
        ? num.tryParse(json['order_value'].toString())
        : num.tryParse(json['order_credit_value'].toString());
    return OrderModel(
      id: json['id'],
      date: json['date'].toString(),
      status: json['status'],
      customerName: json['customer_name'],
      customerPhone: json['customer_phone'].toString(),
      orderValue: orderValue,
      paymentType: json['payment_type'] ?? '',
      important: json['important'] ?? false,
      description: json['description'],
      toll: json['toll'] ?? false,
      waypoints: List<Waypoint>.from(
        json['waypoints'].map(
          (wp) => Waypoint(
            rt: wp['rt'] as int? ?? 0,
            passengers: wp['passengers'] as int? ?? 0,
            address: wp['address'].toString(),
            latitude: double.tryParse(wp['latitude'].toString()) ?? 0.0,
            longitude: double.tryParse(wp['longitude'].toString()) ?? 0.0,
          ),
        ),
      ),
    );
  }

  @override
  String toString() {
    return {
      "id": id,
      "status": status,
      "customerName": customerName,
      "customerPhone": customerPhone,
      "orderValue": orderValue,
      "paymentType": paymentType,
      "important": important,
      "description": description,
    }.toString();
  }

  Map<String, dynamic> toJson() {
    return {
      "waypoints": waypoints.map((e) => e.toJson()).toList(),
      "date": DateTime.parse(date).millisecondsSinceEpoch ~/ 1000,
      "customer_phone": customerPhone,
      "order_value": orderValue,
      "description": description,
    };
  }
}
