class StatisticModel {
  final num count;
  final num credit;
  final num cash;
  final num toll;
  final num total;
  final num percentage;

  StatisticModel({
    required this.count,
    required this.credit,
    required this.cash,
    required this.toll,
    required this.total,
    required this.percentage,
  });
}
