class UserModel {
  final int id;
  final String name;
  final String login;
  final String percent;
  final String? image;
  final int carTypeId;
  final bool inLine;
  final String gender;
  final dynamic calculated;
  final String? long;
  final String? lat;
  // final String? stopTime;
  final int? stopAddressId;
  final String? createdAt;
  final String? updatedAt;
  final String? phone;
  final String? fcmToken;

  UserModel({
    required this.id,
    required this.name,
    required this.login,
    required this.percent,
    required this.image,
    required this.carTypeId,
    required this.inLine,
    required this.gender,
    required this.calculated,
    required this.long,
    required this.lat,
    // required this.stopTime,
    required this.stopAddressId,
    required this.createdAt,
    required this.updatedAt,
    required this.phone,
    required this.fcmToken,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'],
      name: json['name'],
      login: json['login'],
      percent: json['percent'].toString(),
      image: json['image'],
      carTypeId: json['car_type_id'],
      inLine: json['in_line'],
      gender: json['gender'],
      calculated: json['calculated'],
      long: json['long'],
      lat: json['lat'],
      // stopTime: json['stop_time'],
      stopAddressId: json['stop_address_id'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      phone: json['phone'].toString(),
      fcmToken: json['fcm_token'],
    );
  }
}
